//NativeManager.m

//To be modified by project team to include the project name of the Xcode project:
//e.g. #import "gopc-Swift.h"
#import "HANotifApp-Swift.h"

#import "NativeManager.h"

@interface NativeManager ()

@end

@implementation NativeManager

RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(setNativeStorage:(NSString *)key
                  value:(NSString *)value) {
  
  Util *util = [[Util alloc] init];
  [util setNativeStorageWithKey:key value:value];
}

RCT_EXPORT_METHOD(getNativeStorage: (NSString *)key: (RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
  Util *util = [[Util alloc] init];
  NSString *value = [util getNativeStorageWithKey:key];
  
  resolve(value);
}

RCT_EXPORT_METHOD(removeNativeStorage:(NSString *)key) {
  
  Util *util = [[Util alloc] init];
  [util removeNativeStorageWithKey:key];
}

@end

