//Util.swift

import UIKit

@objc class Util: NSObject {
  
  @objc public func setNativeStorage(key: String, value: String) {
    UserDefaults.standard.setValue(value, forKey: key)
  }
  
  @objc public func getNativeStorage(key: String) -> String {
    if let value = UserDefaults.standard.string(forKey: key) {
      return value
    } else {
      return ""
    }
  }
  
  @objc public func removeNativeStorage(key: String) {
    if let _ = UserDefaults.standard.string(forKey: key) {
      UserDefaults.standard.removeObject(forKey: key)
    }
  }
}

