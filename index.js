/**
 * @format
 */


import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import { Text, TextInput } from 'react-native';

AppRegistry.registerComponent(appName, () => App);


Text.defaultProps = {
    ...Text.defaultProps,
    maxFontSizeMultiplier: 1,
  };

  TextInput.defaultProps = {
    ...TextInput.defaultProps,
    maxFontSizeMultiplier: 1,
  };

