import React, { Component } from 'react';
import { Platform, View, ScrollView, Text, StyleSheet, BackHandler, Linking, TouchableWithoutFeedback } from 'react-native';
import BackButton from './components/BackButton';
import { timestampToStr, localizedTitle, localizedAppName, normalizeFontSize, normalizeLineHeight } from './utils';
import HAGo_SPIO from '../HAGo/HAGo_SPIO';
import {
  HAGo_getUndercareColor,
  HAGo_isUndercareMode,
  HAGo_getFontSize
} from '../HAGo/HAGo_authentication';
import I18n from '../language';

const isIOS = Platform.OS === 'ios';
const isAndroid = Platform.OS === 'android';

const detailPageBackButton = BackHandler;

export default class NotificationDetailScreen extends Component {

  // static navigationOptions = ({ navigation }) => {
	// return {
  //     title: localizedTitle(),
  //     headerTintColor: '#fff',
  //     headerStyle: {
  //       backgroundColor: HAGo_isUndercareMode() ? HAGo_getUndercareColor() : 'rgb(255,0,0)',
  //       elevation: 0,
  //       borderBottomWidth:0
  //     },
  //     headerTitleStyle: { 
  //       fontWeight: 'bold',
  //       textAlign: 'center',
  //       fontSize: normalizeFontSize(18),
  //       flex: 1
	//     },
  //     headerLeft: <BackButton/>,
  //     headerRight: (<View></View>)
  //   }
  // }

    componentDidMount() {
      this.props.navigation.setOptions({
        headerTitle: localizedTitle(),
        headerTitleAlign: 'center',
        headerTintColor: '#fff',
        headerStyle: {
          backgroundColor: HAGo_isUndercareMode() ? HAGo_getUndercareColor() : 'rgb(255,0,0)',
          elevation: 0,
          borderBottomWidth:0,
          shadowOpacity: 0
        },
        headerTitleStyle: { 
          fontWeight: 'bold',
          fontSize: normalizeFontSize(18)
        },
          headerLeft: () => (<BackButton/>),
          headerRight: () => (<View></View>)
      })
		if (Platform.OS === 'android') {
			BackHandler.addEventListener('hardwareBackPress', this.backPressed);
		}
	}

    componentWillUnmount() {
		if (Platform.OS === 'android') {
			BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
		}
	}

    backPressed = () => {
        this.props.navigation.goBack();
        return true;
    }

	render() {
    const item = this.props.route.params.item;
    
    const { alert, createDate, data } = item;

    // Error in parsing data, waiting for HA to fix.
    // const jsonData = JSON.parse(data);
    // const content = jsonData.data.content;
    // const message = content ? content.Message : ""
    const message = "";
    
    let contentData = "";
    try {
        const content = JSON.parse(JSON.parse(item.data).data).content;
        contentData = JSON.parse(content);
    } catch (e) {
        //ignore error
        contentData = null;
        console.log('e');
    }
    
    const {
      fill,
      container,
      textPadding,
      separator,
      textContainer
    } = styles;

	return (
			<ScrollView style={[container, fill]}>
        <View>
         {
           HAGo_isUndercareMode() ? <HAGo_SPIO/> : null
         }
        </View>
	 			<View style={separator} />
				<View style={textContainer}>
					<Text style={[{fontWeight: 'bold', color: '#000000', fontSize:normalizeFontSize(25), lineHeight:normalizeLineHeight(25)}]}>{localizedAppName('hago')}</Text>
					<Text style={[{color: '#808080', fontSize:normalizeFontSize(16), lineHeight:normalizeLineHeight(16)}, textPadding]}>{timestampToStr(createDate)}</Text>
					<View style={[separator, { paddingHorizontal: 8, marginTop: 16 }]} />
					<Text style={[{color: '#000000', fontSize:normalizeFontSize(16), lineHeight:normalizeLineHeight(16)}, textPadding]}>{alert}</Text>
                    <Text>
                        {(contentData != null && contentData.content1 != null) ? (<Text style={[{color: '#303030', fontSize:normalizeFontSize(16), lineHeight:normalizeLineHeight(16)}]}>{contentData.content1}</Text>) : null} 
                        {(contentData != null && contentData.urlText1 != null && contentData.urlText1.trim() != '') ? (<TouchableWithoutFeedback onPress={()=>{Linking.openURL(contentData.url1)}}><Text style={[{color: '#0000FF', fontSize:normalizeFontSize(16), lineHeight:normalizeLineHeight(16), textDecorationLine:'underline'}]}>{contentData.urlText1}</Text></TouchableWithoutFeedback>):null}
                        {(contentData != null && contentData.content2 != null) ? (<Text style={[{color: '#303030', fontSize:normalizeFontSize(16), lineHeight:normalizeLineHeight(16)}]}>{contentData.content2}</Text>) : null} 
                        {(contentData != null && contentData.urlText2 != null && contentData.urlText2.trim() != '') ? (<TouchableWithoutFeedback onPress={()=>{Linking.openURL(contentData.url2)}}><Text style={[{color: '#0000FF', fontSize:normalizeFontSize(16), lineHeight:normalizeLineHeight(16), textDecorationLine:'underline'}]}>{contentData.urlText2}</Text></TouchableWithoutFeedback>):null}
                        {(contentData != null && contentData.content3 != null) ? (<Text style={[{color: '#303030', fontSize:normalizeFontSize(16), lineHeight:normalizeLineHeight(16)}]}>{contentData.content3}</Text>) : null} 
                        {(contentData != null && contentData.urlText3 != null && contentData.urlText3.trim() != '') ? (<TouchableWithoutFeedback onPress={()=>{Linking.openURL(contentData.url3)}}><Text style={[{color: '#0000FF', fontSize:normalizeFontSize(16), lineHeight:normalizeLineHeight(16), textDecorationLine:'underline'}]}>{contentData.urlText3}</Text></TouchableWithoutFeedback>):null}
                        {(contentData != null && contentData.content4 != null) ? (<Text style={[{color: '#303030', fontSize:normalizeFontSize(16), lineHeight:normalizeLineHeight(16)}]}>{contentData.content4}</Text>) : null} 
                        {(contentData != null && contentData.urlText4 != null && contentData.urlText4.trim() != '') ? (<TouchableWithoutFeedback onPress={()=>{Linking.openURL(contentData.url4)}}><Text style={[{color: '#0000FF', fontSize:normalizeFontSize(16), lineHeight:normalizeLineHeight(16), textDecorationLine:'underline'}]}>{contentData.urlText4}</Text></TouchableWithoutFeedback>):null}
                        {(contentData != null && contentData.content5 != null) ? (<Text style={[{color: '#303030', fontSize:normalizeFontSize(16), lineHeight:normalizeLineHeight(16)}]}>{contentData.content5}</Text>) : null} 
                        {(contentData != null && contentData.urlText5 != null && contentData.urlText5.trim() != '') ? (<TouchableWithoutFeedback onPress={()=>{Linking.openURL(contentData.url5)}}><Text style={[{color: '#0000FF', fontSize:normalizeFontSize(16), lineHeight:normalizeLineHeight(16), textDecorationLine:'underline'}]}>{contentData.urlText5}</Text></TouchableWithoutFeedback>):null}
                    </Text>
				</View>
			</ScrollView>
		);
	};
}

const styles = StyleSheet.create({
  fill: { flex: 1 },
  container: { backgroundColor: 'white' },
  separator: { height: 1, backgroundColor: '#e5e8e8' },
  textContainer: { padding: 15 },
  textPadding: { paddingTop: 10 },
  contentText:{
    fontSize:16,
    lineHeight:20,
    color:"rgba(48,48,48,0.87)"
  },
  urlTxt:{
    fontSize:16,
    color:"rgb(0,0,255)",
    textDecorationLine: 'underline',
  }
});
