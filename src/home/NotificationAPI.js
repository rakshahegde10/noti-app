import React, { Component, useState } from 'react';
import Base64 from '../HAGo/common/base64';
import _ from 'lodash';
import {
  HAGo_external_parameters,
  HAGo_getDeviceUUID,
  HAGo_getHAAppConfig,
  HAGo_getUndercareColor,
  HAGo_getUserID,
  HAGo_getUndercare_UserID,
  HAGo_isUndercareMode,
  checkExpiresTime,
  HAGo_setNativeStorage,
  HAGo_getNativeStorage,
  HAGo_checkToken,
  HAGo_getInfo,
  HAGo_init,
  HAGo_getLoginStatus
} from '../HAGo/HAGo_authentication';


let token = "";
let cognitoToken = "";
let htoken = "";
let hagotoken = "";


export const fetchNotifications = () => {
  // Retreive notifications 
  return new Promise(async (resolve, reject)=>{
    const config = HAGo_getHAAppConfig();
    cognitoToken = await getCognitoToken(config); // get cognito token
    console.log("token:", cognitoToken)
    if (cognitoToken !== null) {
      try{
        let headers = await getRequestHeaders(cognitoToken);
        console.log("headers:", headers)
        let body = getRequestBody(config);
        console.log("body:", body)
        let notificationUrl = config["notification-retrieve-api"];
         const response = await fetch(notificationUrl,         // API call
          {
            method: 'POST',
            headers: headers,
            body: body
          })
          
            if (response.ok) {
              const responseObj = await response.json();
              resolve(responseObj)
            } else {
              throw new Error('Something went wrong');
            }
      } catch (e) {
          // something went wrong
          console.log('error');
          reject(e); 
      }
    }
    })
  
}

  async function getCognitoToken(config) {
    // Get Cognito token
    const authorization = getAuthorization(config);
    const cognitoUrl = config["cognito-domain"];
    if(token === ""){
    cognitoToken = await fetch(cognitoUrl,
      {
        method: 'POST',
        headers:
        {
          'authorization': `Basic ${authorization}`,
          'content-type': 'application/x-www-form-urlencoded',
        },
        body: formatToUrlEncoded({ 'grant_type': 'client_credentials' }),
      })
      .then((response) => {
        if (response.ok) {
          
          return response.json();
        } else {
          throw new Error('Something went wrong');
        }
      })
      .then((responseJson) => responseJson.access_token
      )
    token = cognitoToken;
    }
    if(token !== ""){
    let expiry = parseJwt(token);   // check cognito token expiry time
    let exp = (Math.floor((new Date).getTime() / 1000)) >= expiry;
    if(exp === false){
      cognitoToken = token
    } else {
      cognitoToken = await fetch(cognitoUrl,
        {
          method: 'POST',
          headers:
          {
            'authorization': `Basic ${authorization}`,
            'content-type': 'application/x-www-form-urlencoded',
          },
          body: formatToUrlEncoded({ 'grant_type': 'client_credentials' }),
        })
        .then((response) => {
          if (response.ok) {
            
            return response.json();
          } else {
            throw new Error('Something went wrong');
          }
        })
        .then((responseJson) => responseJson.access_token
        )
        token = cognitoToken;
    }
  }
    return cognitoToken;
  }

  async function getHagoToken() {
    // Get HAGo token
    const config = HAGo_getHAAppConfig();
    const result = await HAGo_external_parameters(config["hago-api-client-id"]);
    const formData = result.hago_parameters._parts;

    const data = JSON.stringify({
      content: formData[0][1],
      en_key: formData[1][1]
    })

    return Base64.btoa(data);
  }

  function parseJwt (token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(Base64.atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
  
    var pay = JSON.parse(jsonPayload);
    var exp = pay.exp;
    return exp;
  };


  function getAuthorization(config) {
    let clientId = config["cognito-hago-client-id"];
    let clientSecret = config["cognito-hago-client-secret"];
    return Base64.btoa(`${clientId}:${clientSecret}`);
  }

  async function getRequestHeaders(cognitoToken) {
    // Get Headers for token
    let headers;
    if (HAGo_isUndercareMode()) {
      headers = {
        'Content-Type': 'application/json',
        'Authorization': cognitoToken
      } 
     }  else {
      headers = {
      'Content-Type': 'application/json',
      'Authorization': cognitoToken,
      'targetId': HAGo_getDeviceUUID()
    };
  }
    const loginStatus = HAGo_getLoginStatus();
    if(loginStatus === 'Login' ) {
      const userId = HAGo_isUndercareMode() ? HAGo_getUndercare_UserID() : HAGo_getUserID();
    if (userId !== null && userId !== "" && userId !== undefined) {
      if(htoken === ""){
      hagotoken = await getHagoToken();
      htoken = hagotoken;
      }
      if(htoken !== ""){
        let exp = checkExpiresTime(htoken);
        if(exp === true){
          hagotoken = await getHagoToken();
        }else{
           hagotoken = htoken;
        }
      }
      headers = { ...headers, userId, hagotoken };
    }}
    return headers;
  }

  function getRequestBody(config) {
    // Get Body for token
     let data;
      
    if (HAGo_isUndercareMode()) {
      data = {
      applicationId: config["aws_hago_applicationId"]
    };
   } else {
      data = {
      applicationId: config["aws_hago_applicationId"],
      subApplicationId: '*',
      targetId: HAGo_getDeviceUUID(),
      loginRequired: false,
      // restartKey: '{"createDate":1580801654000}' optional param for filtering notifications, can be removed. 
    };
  }
    const loginStatus = HAGo_getLoginStatus();
    if(loginStatus === 'Login' ) {
    const userId = HAGo_isUndercareMode() ? HAGo_getUndercare_UserID() : HAGo_getUserID();
      if (userId !== null && userId !== "" && userId !== undefined) {
        data = { ...data, userId, loginRequired: true };
      }
  }

    return JSON.stringify(data);
  }

  
  function formatToUrlEncoded(bodyString) {
      var formBody = [];
      for (var property in bodyString) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(bodyString[property]);
        formBody.push(encodedKey + "=" + encodedValue);
      }
      formBody = formBody.join("&");
      return formBody;
    }


    export const updateNotifications = async (id, userId) => {
      // Update API logic
        const config = HAGo_getHAAppConfig();
    
        const cognitoToken = await getCognitoToken(config); 
     
        if (cognitoToken !== null) {
          try{
            let headers = {
              'Content-Type': 'application/json',
              'Authorization': cognitoToken,
              'targetId': HAGo_getDeviceUUID()
            };
        
            if (userId !== null && userId !== "" && userId !== undefined) {
              const hagotoken = await getHagoToken();
        
              headers = { ...headers, userId, hagotoken };
            }

            let body = JSON.stringify({
              'status' : 'Read'
             });
            
            let updateUrl = config["notification-update-api"] + '/' + id;
            if (userId !== ""){
            const response = await fetch(updateUrl,      // API call
              {
                method: 'PATCH',
                headers: headers,
                body: body
              })
            }
          } catch (e) {
              // something went wrong
              console.log('error'); 
          }
        }

    }

    export const fetchNotificationsWithCount = async () => {   
      // retreive notifications by API call. filter notifications for "New"/Unread notifications.
      // set Native storage 'notificationApi_unreadCount'
      const notifications = await fetchNotifications();
      const unreadNotifications = notifications.filter(item => {if(item.userId !== undefined){
                                                                    if(item.status !== 'Read'){
                                                                          return item }
                                                                          }
                                                                            });
      let count = 0;
      count = unreadNotifications.length;
      count = count > 99 ? '99+' : count;
      let unreadCount = count.toString();
      await HAGo_setNativeStorage('notificationApi_unreadCount', unreadCount);
    }

    export const fetchUnreadFlag = async () => {   
      // retreive notifications by API call. filter notifications for "New"/Unread Personal and General notifications.
      // set Native storage 'notificationApi_unreadCountFlag'

      let notifications = await fetchNotifications();
      // Personal Notifications logic
      let unreadNotifications = notifications.filter(item => {if(item.userId !== undefined){
                                                                    if(item.status !== 'Read'){
                                                                          return item }
                                                                          }
                                                                            });
      let count = 0;
      count = unreadNotifications.length;
      let unreadCountFlag = ''
      unreadCountFlag = count > 0 ? true : false;
      unreadCountFlag = unreadCountFlag.toString();
      
      // General Notifications logic
      let msgs = [];
      await HAGo_getNativeStorage('general_messages')  
      .then(req => JSON.parse(req))
      .then(json => msgs = json)
      .catch(error => console.log('error!!!'));
      let unreadGeneralNotifications = notifications.filter(item => { if(item.userId === undefined && item.targetId === undefined){
                                                                return item
                                                                      } });
      let unreadCountFlagGen = '';
      if (msgs === null || msgs.length === 0 ) {  
        unreadCountFlagGen = (unreadGeneralNotifications.length > 0) ? true : false;
        unreadCountFlagGen = unreadCountFlagGen.toString();
      } else {
        let found = '';
        let list = _.map(unreadGeneralNotifications, item => {
        for ( let key in msgs) {
          var obj = msgs[key];
          found = 'New';
              if (item.id === obj.id) {
                  found = 'Read';
                  break;
              }
            }
            return found;
        })
        unreadCountFlagGen = list.includes('New') ? true : false;
        unreadCountFlagGen = unreadCountFlagGen.toString();
      }

      let unreadFlag = (unreadCountFlag === 'true' || unreadCountFlagGen === 'true') ? true : false;
      unreadFlag = unreadFlag.toString();

      await HAGo_setNativeStorage('notificationApi_unreadCountFlag', unreadFlag);
    }


        