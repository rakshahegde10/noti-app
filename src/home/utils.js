import moment from 'moment';
import I18n  from '../language';
import { HAGo_getFontSize } from '../HAGo/HAGo_authentication';
import { Platform } from 'react-native';

const HA_apps = Object.freeze({
  hago: 'hago',
  appointment: 'appointment',
  bookha: 'bookha',
  payha: 'payha',
  rehab: 'rehab',
  medication: 'medication',
  easyq: 'easyq',
  haconnect: 'haconnect',
  telecare: 'telecare'
});

export function localizedTitle() {
  return I18n.t('title');
}
export function localizedsubTitle() {
  return I18n.t('subTitle');
}
export function localizedsearchResults() {
  return I18n.t('searchResults');
}
export function localizednoMessages() {
  return I18n.t('noMessages');
}

export function localizedAppName(appId) {
  const { hago, appointment, bookha, payha, rehab, medication, easyq, haconnect, telecare} = HA_apps;

    if (!appId || appId.includes(hago)) {
      return I18n.t(`app_icon_name.${hago}`)
    } else if (appId.includes(appointment)) {
      return I18n.t(`app_icon_name.${appointment}`)
    } else if (appId.includes(bookha)) {
      return I18n.t(`app_icon_name.${bookha}`)
    } else if (appId.includes(payha)) {
      return I18n.t(`app_icon_name.${payha}`)
    } else if (appId.includes(rehab)) {
      return I18n.t(`app_icon_name.${rehab}`)
    } else if (appId.includes(medication)) {
      return I18n.t(`app_icon_name.${medication}`)
    } else if (appId.includes(easyq)) {
      return I18n.t(`app_icon_name.${easyq}`)
    } else if (appId.includes(haconnect)) {
      return I18n.t(`app_icon_name.${haconnect}`)
    } else if (appId.includes(telecare)) {
      return I18n.t(`app_icon_name.${telecare}`)
    } else {
      return "";
    }
}

export function timestampToStr(timestamp) {
    return I18n.t('messageCreateDate.prefixString') 
	     + moment(timestamp).format(I18n.t('messageCreateDate.dateTimeFormat' + (new Date(timestamp).getHours()<12?'AM':'PM')));
}

export function normalizeFontSize(fontsize) {
    let fontScale = getFontScale();
    return Math.round(fontsize*fontScale);
}

export function normalizeLineHeight(fontsize) {
    if (Platform.OS === 'android' && Platform.Version >= 28){
       return parseInt((fontsize * 1.4), 10);
    } else {
       let fontScale = getFontScale();
       return Math.round(fontsize*fontScale*1.3);
    }
}

export function getFontScale() {
    let fontSize = HAGo_getFontSize()

    var fontScale = 1

    switch(fontSize) {
        case 'small':
            fontScale = 0.8
            break;
        case 'normal':
            fontScale = 1
            break;
        case 'large':
            fontScale = 1.2
            break;
    }

    return fontScale;
}

export function appImageIconrequireString(appName){
    let appIcons = {
      'hago': require('../HAGo/images/app_icon/ic_hago.png'),
      'rehab': require('../HAGo/images/app_icon/ic_rehab.png'),
      'appointment': require('../HAGo/images/app_icon/ic_appointment.png'),
      'payha': require('../HAGo/images/app_icon/ic_payha.png'),
      'bookha': require('../HAGo/images/app_icon/ic_bookha.png'),
      'medication': require('../HAGo/images/app_icon/ic_medication.png'),
      'gopc_booking': require('../HAGo/images/app_icon/ic_gopc_booking.png'),
      'easyq': require('../HAGo/images/app_icon/ic_easyq.png'),
      'haconnect': require('../HAGo/images/app_icon/ic_haconnect.png'),
      'telecare': require('../HAGo/images/app_icon/ic_telecare.png')
    };

    url = appIcons[appName];

    return url;
}

