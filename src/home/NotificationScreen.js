
import React, { Component } from 'react';
import {
  Platform,
  FlatList, 
  StyleSheet,
  Text,
  View,
  BackHandler,
  RefreshControl,
  ScrollView,
  TouchableOpacity,
  TextInput,
  Keyboard,
  Linking
} from 'react-native';
import Base64 from '../HAGo/common/base64';
import  HAGo_BackButton, {
  HAGo_external_parameters,
  HAGo_getDeviceUUID,
  HAGo_getHAAppConfig,
  HAGo_getUndercareColor,
  HAGo_getUserID,
  HAGo_isUndercareMode,
  navigateToOtherApp,
  HAGo_backAction,
  HAGo_setNativeStorage,
  HAGo_getNativeStorage,
  HAGo_getLoginStatus,
  HAGo_getFontSize
} from '../HAGo/HAGo_authentication';
import NotificationItem from './components/NotificationItem';
import { localizedTitle, normalizeFontSize, normalizeLineHeight, localizedAppName, localizedsubTitle } from './utils';
import I18n from '../language';
import {  Badge } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialIcons';
import _ from 'lodash';
import SearchOption from './components/SearchOption';
import { fetchNotifications, updateNotifications } from './NotificationAPI';
import HAGo_SPIO from '../HAGo/HAGo_SPIO';

const isIOS = Platform.OS === 'ios';
const isAndroid = Platform.OS === 'android';

export default class NotificationScreen extends Component {
  constructor(props) {
    super(props)

    this.state = {
      notifications: [],
      error: null,
      refreshing: false,
      notificationRead: false,
      list: [],
      displaySearch: false,
      icon: true,
      dropList: "",
      genList: [],
      gen: []
    }
    Icon.loadFont()
   }

      async componentDidMount() {
        this.props.navigation.setOptions({ headerTitle: localizedTitle(),
          headerTintColor: '#fff',
          headerStyle: {
            backgroundColor: HAGo_isUndercareMode() ? HAGo_getUndercareColor() : 'rgb(255,0,0)',
            elevation:0,
            borderBottomWidth: 0,
            shadowOpacity: 0
            },
            headerTitleStyle: {
              fontWeight: 'bold',
              fontSize: normalizeFontSize(18),
            },
            headerTitleAlign: 'center',
            headerLeft: () => (<HAGo_BackButton accessible={true}
                                          testID='LeftButton'
                                          accessibilityLabel="headerLeftButton"/>),
            headerRight: () => (<View></View>)})


            if (Platform.OS === 'android') {
              BackHandler.addEventListener('hardwareBackPress', this.backPressed);
            }
            this.setState({
              refreshing: true
            })
            
            try {
              // Retrieve notifications from API
              const notifications = await fetchNotifications();  
              console.log(notifications)
              this.setState({
                notifications,
                error: null,
                refreshing: false
              });
            } catch (error) {
              this.setState({
                notifications: [],
                error,
                refreshing: false
              });

            }
            await HAGo_getNativeStorage('general_messages')
            .then(req => JSON.parse(req))
            .then(json => this.setState({gen : json}))
            .catch(error => console.log('error!'));
            this.state.gen === null ? null : this.setState({ genList : this.state.gen })
            let notif = this.state.notifications.filter(item => {
            if(item.userId === undefined){
              let list;
                list = { id : item.id }
                this.setState({
                genList: [...this.state.genList, list]
                });
            }
          }) 
              let myData = this.state.genList
              let data = Array.from(new Set(myData.map(JSON.stringify))).map(JSON.parse);
              await HAGo_setNativeStorage('general_messages', JSON.stringify(data));     
      }


      backPressed = () => {
        HAGo_backAction();
            return true;
      }
      

        goToDetails = async (item) => {
        // go to item detail 
              let contentData = '';
              try {
                  const content = JSON.parse(JSON.parse(item.data).data).content;
                  contentData = content;
              } catch (e) {
                  contentData;
                  console.log(e);
              }    
              try {
                  if (contentData !== null && Object.keys(contentData).includes('nc.openurl')) {
                      const url = Object.values(contentData)[0];
                      return Linking.openURL(url); 
                  } else if (item.subApplicationId == null || item.subApplicationId.includes("hago")) {
                      return this.props.navigation.navigate('Details', { item });
                  } else {
                      navigateToOtherApp("notification", item.subApplicationId, JSON.parse(JSON.parse(item.data).data).content);
                  }
              } catch (err) {
                console.log(err);
              }
      }
      
      refresh = async() => {
        // refresh notifications
        this.setState({refreshing : true})
        try {
          const notifications = await fetchNotifications();
          this.setState({
            notifications,
            error: null,
            refreshing: false,
            list: []
          });
        } catch (error) {
          this.setState({
            notifications: [],
            error,
            refreshing: false
          });
        }
        await HAGo_getNativeStorage('general_messages')
            .then(req => JSON.parse(req))
            .then(json => this.setState({gen : json}))
            .catch(error => console.log('error!'));
            this.state.gen === null ? null : this.setState({ genList : this.state.gen })
            let notif = this.state.notifications.filter(item => {
            if(item.userId === undefined){
              let list;
                list = { id : item.id }
                this.setState({
                genList: [...this.state.genList, list]
                });
            }
          }) 
              let myData = this.state.genList
              let data = Array.from(new Set(myData.map(JSON.stringify))).map(JSON.parse);
              await HAGo_setNativeStorage('general_messages', JSON.stringify(data));     
      }

      refreshControl() {
        const { refreshing } = this.state;
        
        return (
          <RefreshControl
            refreshing={refreshing}
            onRefresh={this.refresh}
            colors={['#2B70AD']}
          />
        );
      };

      setRefresh(bool){
        this.setState({
          refreshing: true
        })
      }

      setNotifRead(bool) {
        // state for Unread notification 
        if(HAGo_getLoginStatus()) 
        this.setState({
          notificationRead: bool,
        });
      }
      
      setReadCount(id) {
        // Unread notification local list
        if(!this.state.list.includes(id) && HAGo_getLoginStatus() && !HAGo_isUndercareMode()){
        this.setState({
          list: [...this.state.list, id],
        });
      }
      }

      update = async(item) => {
        // update "Status" to "Read"
        const userId = item.userId;
          if(HAGo_isUndercareMode()){
            console.log('In Undercare mode!')
          } else 
          {
                const loginStatus = HAGo_getLoginStatus();
                if(item.status === 'New' && userId !== undefined && loginStatus === 'Login' ){
                await updateNotifications(item.id, userId);   // update API call
                } if (userId === undefined){ 
                  console.log('General Message!!')
                  return;
                } else {
                  console.log('already read!!')
                  return;
          }
          }
      }
    
      renderItem({item}) {
        // FlatList item component 
        return <NotificationItem item={item} onPress={() => {this.goToDetails(item);this.setNotifRead(true);this.update(item)}}
                      notificationRead={this.state.notificationRead}
                      readNotifCountFn={(id)=>{this.setReadCount(id)}}
                      list={this.state.list} 
                      /> ;
      }
      
      renderPlaceholder(message) {
        const { fill, center, container } = styles;
        console.log("message", message)
        return  (
          <ScrollView
            style={fill}
            contentContainerStyle={[
              { flexGrow: 1},
              center,
              container
            ]}
            refreshControl={this.refreshControl()}>
            <Text style={{fontSize:normalizeFontSize(16), fontSize:normalizeLineHeight(14), textAlign:'center', padding:15}}>{message}</Text>
          </ScrollView>
        );
      }
      

      setDisplaySearch() {
      // search bar display
        if(this.state.notifications.length === 0){
        this.setState({
          displaySearch: this.state.displaySearch
        }) } else {
          this.setState({
            displaySearch: !this.state.displaySearch
          }) 
        }
        
      }


      setIcon() {
      // search icon display
        const subApp = _.map(this.state.notifications, (item) => { 
          let appName = item.subApplicationId; 
          if(appName === "hk.org.ha.hago" || appName === "telecare"){
            return appName = 'hago';
          }else{
            return appName;
          }
        })
        const data = Array.from(new Set(subApp)); //search bar filter data

        if(this.state.notifications.length === 0){
          this.setState({
            icon : this.state.icon
          }) } else {
            this.setState({
              icon: !this.state.icon,
              dropList: data
            }) 
          }
      }

      renderSearch() {
        const {row, badge} = styles;
        const { icon, notifications, list, count } = this.state;
        // unread notification count calculation in NC
        let arr = notifications.filter(item => {
                                      if(item.userId !== undefined){
                                        if(item.status !== 'Read'){
                                        return item }
                                        }
                                          });

        let badgeValue = 0
        if(list.length === 0){
          badgeValue = arr.length 
        }else {
          badgeValue = arr.length - list.length
        }
        
        let unreadCount = badgeValue.toString();
        HAGo_setNativeStorage('notificationApi_unreadCount', unreadCount);

        let textsize = HAGo_getFontSize()
        console.log("text size:", textsize)
        
        // Logic to display Undercare user name, "My Notifications" header, Unread notifications Badge count and search icon
        return(
          <View >
            {
              HAGo_isUndercareMode() ? <HAGo_SPIO/> : null
            }
          <View style={row}>
            <View style={badge}> 
              <Text style={{fontSize:normalizeFontSize(17),lineHeight:normalizeLineHeight(17),fontWeight:'bold'}}>
                {localizedsubTitle()}  </Text>
                    <Badge status="error" 
                      value={badgeValue} 
                      containerStyle={{ marginLeft: 7}} 
                      badgeStyle={{borderColor: 'rgb(255,0,0)'}} 
                      textStyle={{fontSize:normalizeFontSize(12.5), fontWeight:'bold', textAlign:'center'}}
                      accessible={true}
                      testID='unreadCountBadge'
                      accessibilityLabel="unreadCountNotiBadge"
                      />
            </View>
            {  
              icon === true ? 
              <View>
            <Icon name='search' onPress={()=>{this.setDisplaySearch();this.setIcon()}} 
            size={textsize === 'large' ? 26 : 20}
            accessible={true}
            testID='searchIcon' 
            accessibilityLabel="searchBarIcon"/>
            </View> : null
            }
          </View>
          </View>
        );
          }


          

      renderContent() {
        // Notification List and Search Bar component 
        const { notifications, refreshing, miniApp, dropList, displaySearch } = this.state;
        const searchNotifications = notifications;
        const config = HAGo_getHAAppConfig();

        return notifications.length === 0 && !refreshing
          ? this.renderPlaceholder(config["no_message_" + I18n.locale])
          : (
          <View style={{flex: 1}}>
            {
              displaySearch == true ?
          <SearchOption setIconFn={(bool)=>{this.setIcon(bool)}} 
          setDisplaySearchFn={(bool)=>{this.setDisplaySearch(bool)}} 
          dropList={dropList}
          searchNotifications={searchNotifications}
          list={this.state.list}
          notificationRead={this.state.notificationRead}
          displaySearch={this.state.displaySearch}
          goToDetailFn={(item)=>{this.goToDetails(item)}}
          setNotifReadFn={(bool)=>{this.setNotifRead(bool)}}
          readNotifCountFn={(id)=>{this.setReadCount(id)}}
          /> 
            : 
          <FlatList
            ListFooterComponent={<View style={{ paddingBottom: 50 }}></View>}
            data={notifications}
            keyExtractor={(item) => item.id}
            renderItem={this.renderItem.bind(this)}
            refreshControl={this.refreshControl()}
            />
            }
          </View>
          );
      }

      render() {
        const { error } = this.state;
      const config = HAGo_getHAAppConfig();

        if (error) {
          return this.renderPlaceholder(config["no_service_" + I18n.locale]);
        }

        const { fill, container } = styles;

        return (
          <View style={[fill, container]}>
            { this.renderSearch() }
            { this.renderContent() }
          </View>
        );
      }
  }


const styles = StyleSheet.create({
  fill: { flex: 1 },
  row: { flexDirection: 'row' },
  center: {
    alignItems: 'center',
    justifyContent: 'center'
  }, 
  container: { backgroundColor: 'white' },
  row:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10,
    backgroundColor: '#D3D3D3'
  },
  button: {
  color: 'black'
  },
  badge:{
    flexDirection: 'row',
    alignItems: 'center'

  },
  row1:{
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  search:{
    width: 340
  },
  container1: {
    justifyContent: 'center',
    alignItems: 'center',
    // position:'relative'   
  },
  section: {
    flexDirection: 'row',
    // justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderWidth: 0.5,
    borderColor: '#000',
    height: 40,
    borderRadius: 5
  }
})

