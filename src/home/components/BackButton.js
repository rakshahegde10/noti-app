import { View, Text, StyleSheet, Platform, TouchableOpacity, Image } from "react-native";
import React from 'react';
import { withNavigation } from '@react-navigation/compat';


class BackButton extends React.Component {
    render() {
        return (
            <TouchableOpacity style={{width:38,height:38}} onPress={ () => {
                this.props.navigation.goBack()
            }}>
				<Image source={require('../../images/Back_button.png')} style={{width: 38, height: 38, }}/>
			</TouchableOpacity>
        )
    }
}


export default withNavigation(BackButton);
