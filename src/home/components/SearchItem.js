import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet } from 'react-native';
import { timestampToStr, localizedAppName, normalizeFontSize, normalizeLineHeight, appImageIconrequireString } from '../utils';
import { Badge } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialIcons';
import _ from 'lodash';


export default class SearchItem extends Component {

  constructor(props) {
    super(props)

    this.state = {
     arrowClicked: false,
     lastChar: false,
     initSearch: ""
    }
   }

  getIcon(appId) {
    if (!appId || appId.includes('hago')) {
      return appImageIconrequireString('hago');
    } else {
      return appImageIconrequireString(appId);
    }
  }

  handlePress(){
   return this.props.onPress();
  }
     
  setList(id, userId, status) {
  // unread notification badge display 
    if(userId !== undefined){
      if(status !== 'Read') {
    return this.props.readNotifCountFn(id);
      }}
  
}

  setFullLength(){
  // expand/collapse display   
    this.setState({
      arrowClicked: !this.state.arrowClicked ,
    })
  }

  setFullLengthUp(bool){
  // expand/collapse display  
    this.setState({
      arrowClicked: !this.state.arrowClicked,
      initSearch: 0
    })

  }

  setFullLengthSearch(bool){
  // expand/collapse display  
    this.setState({
      arrowClicked: bool
    })

  }

  measureView(event, lines) {
  // measure notification's number of lines 
    const line = event.nativeEvent.lines.length
    if (line > 3) {
        this.setState({ lastChar: true})
      }
  }

  escape = function( value ) {
    return value.replace(/[\-\[\]{}()*+?.,\\\^$%#\s]/g, "\\$&");
}


  getHighlightedText(text){
    // Highlight logic 
    const {value} = this.props;
    
    if(value == "" || value == null || value == 0){
      return <Text> {text} </Text>
    }
    else{
    const words = value.split(/\s+/g).filter(word => word.length);
    const pattern = words.join('|');
    const tex = escape(pattern);
    const re = new RegExp(tex, 'gi')
    const children = [];
    let before, highlighted, match, pos = 0;
    const matches = text.match(re);

    if(matches != null){
    for( match of matches ){
      match = re.exec(text)
      if(pos < match.index) {
        before = text.substring(pos, match.index);
        if(before.length){
          children.push(before)
        }
      }
      highlighted = <Text style={{ backgroundColor: 'coral'}} key={match.index + match[0].length}>{match[0]}</Text> 
      children.push(highlighted);    
      pos = match.index + match[0].length;
    }
  }

    if(pos < text.length){
      const last = text.substring(pos);
      children.push(last);
    }
    return <Text>{children}</Text>
  }
}
 
  render() {
    const {
      row,
      badge,
      image,
      icon,
      textContainer,
      title,
      subTitle,
      time,
      separator
    } = styles;
  
    const { item, notificationRead, value, list } = this.props;
    const { appId, subApplicationId, alert, createDate, id, status, userId } = item;
    const applicationId = subApplicationId ? subApplicationId : appId;
    const {arrowClicked, lastChar, initSearch} = this.state;
 

    return (
       <View>
       <TouchableOpacity onPress={()=>{this.handlePress();this.setList(id, userId, status)}}>
        <View>
          <View style={row}>
                
          {  
             userId !== undefined ?
             status !== 'Read' ?
             notificationRead === false || list.includes(id) === false ?
             <View style={badge} >
               <Badge status="error" accessible={true} testID='redDotInSearch' accessibilityLabel="redDotBadgeInSearch"/>
             </View> : null 
             : null
             : null
            }
            
            <View style={image}>
              <Image style={icon} source={this.getIcon(applicationId)} />
            </View>
            <View style={textContainer}>
            <View style={{opacity:0, height:0}}> 
              <Text style={[subTitle,{fontSize:normalizeFontSize(15),lineHeight:normalizeLineHeight(15)}]} onTextLayout={(event, lines) => this.measureView(event, lines)}>{alert}</Text>
              </View>
              <Text style={[title,{fontSize:normalizeFontSize(16),lineHeight:normalizeLineHeight(16)}]}>{localizedAppName(applicationId)}</Text>
              { arrowClicked === true ? 
              <Text style={[subTitle,{fontSize:normalizeFontSize(15),lineHeight:normalizeLineHeight(15)}]}>{value !== "" ? this.getHighlightedText(alert) : alert}</Text> 
              : 
              value !== "" && initSearch === "" ?
              <Text onLayout={()=>{this.setFullLengthSearch(true)}} onTextLayout={(event, lines) => this.measureView(event, lines)} style={[subTitle,{fontSize:normalizeFontSize(15),lineHeight:normalizeLineHeight(15)}]}>{value !== "" ? this.getHighlightedText(alert) : alert}</Text> 
              :
              <Text numberOfLines={3} onTextLayout={(event, lines) => this.measureView(event, lines)} style={[subTitle,{fontSize:normalizeFontSize(15),lineHeight:normalizeLineHeight(15)}]}>{value !== "" ? this.getHighlightedText(alert) : alert}</Text>   
                         
            }
              <View style={{flexDirection: 'row',alignItems: 'center',justifyContent: 'space-between'}}>
              <View style={{width:200}}>
              <Text style={[time,{fontSize:normalizeFontSize(14),lineHeight:normalizeLineHeight(14)}]}>{timestampToStr(createDate)}</Text>         
              </View>
              <View style={{position:'absolute', right:3}}>
            {  lastChar === true ?
                arrowClicked === true ? 
                <Icon name='arrow-drop-up' size={33} onPress={()=>{this.setFullLengthUp()}} accessible={true} testID='collapseIcon' accessibilityLabel="collapseNotiIcon"/>
                : 
                <Icon name='arrow-drop-down' size={33} onPress={()=>{this.setFullLength()}} accessible={true} testID='expandIcon' accessibilityLabel="expandNotiIcon"/>
                 : null
            } 
            </View> 
             </View>
            </View>
          </View>

          <View style={separator}></View>
        </View>
      </TouchableOpacity>
     </View>
    );
  }
}

const styles = StyleSheet.create({
  row: { 
	flexDirection: 'row', 
    alignItems: 'flex-start',
    marginLeft: 15,
    marginRight: 15,
    paddingTop: 5,
    paddingBottom: 10
  },
  badge: {
    position: 'absolute',
    top: 34, 
    },
  image: {
    flex: 1, 
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 5
  },
  icon: {
    width: 60,
    height: 60
  },
  textContainer: {
    flex: 3,
    paddingLeft: 15
  },
  title: {
    flex: 1,
    color: '#000000',
	fontWeight: 'bold'
  },
  subTitle: {
    flex: 1,
    color: '#000000'
  },
  time: {
    flex: 1,
    color: '#808080'
  },
  separator: {
    height: 1,
    marginLeft: 15,
    marginRight: 15,
    backgroundColor: "#e5e8e8"
  }
})


