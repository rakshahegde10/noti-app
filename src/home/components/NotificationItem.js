import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet } from 'react-native';
import { timestampToStr, localizedAppName, normalizeFontSize, normalizeLineHeight, appImageIconrequireString } from '../utils';
import { Badge } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialIcons';
import _ from 'lodash';


class NotificationItem extends Component {
  constructor(props) {
    super(props)

    this.state = {
     arrowClicked: false,
     lastChar: false
    }
   }

  getIcon(appId) {
    if (!appId || appId.includes('hago')) {
      return appImageIconrequireString('hago');
    } else {
      return appImageIconrequireString(appId);
    }
  }

  handlePress(){
   return this.props.onPress();
  }
     
  setList(id, userId, status) {
    // unread notification badge display
    if(userId !== undefined){
      if(status !== 'Read') {
    return this.props.readNotifCountFn(id);
    }}
  
}

  setFullLength(bool){
   // expand/collapse display  
    this.setState({
      arrowClicked: !this.state.arrowClicked
    })
  }
  
  measureView(event, lines) {
   // measure notification's number of lines
    const line = event.nativeEvent.lines.length
    if (line > 3) {
        this.setState({ lastChar: true})
      }
  }

  render() {
    const {
      row,
      badge,
      image,
      icon,
      textContainer,
      title,
      subTitle,
      time,
      separator
    } = styles;
  
    const { item, notificationRead, list } = this.props;
    const { appId, subApplicationId, alert, createDate, id, status, userId } = item;
    const applicationId = subApplicationId ? subApplicationId : appId;
    const {arrowClicked, lastChar} = this.state;

    return (
       <View>
       <TouchableOpacity onPress={()=>{this.handlePress();this.setList(id, userId, status)}}>
        <View>
          <View style={row}>
                
            {  
             userId !== undefined ?
             status !== 'Read' ?
             notificationRead === false || list.includes(id) === false ?
             <View style={badge}>
               <Badge status="error" accessible={true} testID='redDot' accessibilityLabel="redDotBadge"/>
             </View> : null 
             : null
             : null
            }
            
            <View style={image}>
              <Image style={icon} source={this.getIcon(applicationId)} />
            </View>
            <View style={textContainer}>
            <View style={{opacity:0, height:0}}> 
              <Text style={[subTitle,{fontSize:normalizeFontSize(15),lineHeight:normalizeLineHeight(15)}]} onTextLayout={(event, lines) => this.measureView(event, lines)}>{alert}</Text>
              </View>
              <Text style={[title,{fontSize:normalizeFontSize(16),lineHeight:normalizeLineHeight(16)}]}>{localizedAppName(applicationId)}</Text>
              { arrowClicked === true ? 
              <Text style={[subTitle,{fontSize:normalizeFontSize(15),lineHeight:normalizeLineHeight(15)}]}>{alert}</Text>
              :
              <Text numberOfLines={3} onTextLayout={(event, lines) => this.measureView(event, lines)} style={[subTitle,{fontSize:normalizeFontSize(15),lineHeight:normalizeLineHeight(15)}]}>{alert}</Text>
              }
               <View style={{flexDirection: 'row',alignItems: 'center',justifyContent: 'space-between'}}>
              <View style={{width:200}}>
              <Text style={[time,{fontSize:normalizeFontSize(14),lineHeight:normalizeLineHeight(14)}]}>{timestampToStr(createDate)}</Text>         
              </View>
              <View style={{position:'absolute', right:3}}>
            {  lastChar === true ?
                arrowClicked === true ? 
                <Icon name='arrow-drop-up' size={33} onPress={()=>{this.setFullLength()}} />
                : 
                <Icon name='arrow-drop-down' size={33} onPress={()=>{this.setFullLength()}} />
                : null
            } 
              </View> 
             </View>
            </View>
          </View>
          <View style={separator}></View>
        </View>
      </TouchableOpacity>
     </View>
    );
  }
}

const styles = StyleSheet.create({
  row: { 
	flexDirection: 'row', 
    alignItems: 'flex-start',
    marginLeft: 15,
    marginRight: 15,
    paddingTop: 5,
    paddingBottom: 10
  },
  badge: {
    position: 'absolute',
    top: 34, 
    },
  image: {
    flex: 1, 
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 5
  },
  icon: {
    width: 60,
    height: 60
  },
  textContainer: {
    flex: 3,
    paddingLeft: 15
  },
  title: {
    flex: 1,
    color: '#000000',
	fontWeight: 'bold'
  },
  subTitle: {
    flex: 1,
    color: '#000000'
  },
  time: {
    flex: 1,
    color: '#808080'
  },
  separator: {
    height: 1,
    marginLeft: 15,
    marginRight: 15,
    backgroundColor: "#e5e8e8"
  }
})

export default NotificationItem;
