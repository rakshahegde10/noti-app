import React, { Component } from 'react';
import { Text, View, Image, StyleSheet, TouchableHighlight, TouchableOpacity, FlatList,TextInput, RefreshControl } from 'react-native';
import { appImageIconrequireString, normalizeFontSize, normalizeLineHeight, localizedAppName, localizedsearchResults, localizednoMessages} from '../utils';
import {  ListItem, Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialIcons';
import SearchItem from './SearchItem';
import _ from 'lodash';
import { updateNotifications } from '../NotificationAPI';
import Label from './label';

let vKey = 0

export default class SearchOption extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
          temp: [],
          notif: [],
          fNotif:[],
          miniApp: "",
          value: "",
          val: "",
          flat: false,
          refreshing: false,
        }
       }


       componentDidMount() {
         const{searchNotifications}= this.props;
        this.setState({
          temp: searchNotifications,
          notif: searchNotifications
        })
    }

        setDisplaySearch(bool){
        // search bar display
          return this.props.setDisplaySearchFn(bool);
        }

        setIcon(bool){
        // search icon display
          return this.props.setIconFn(bool);
        }

        renderDropDown(bool) {
        // filter list display
          this.setState({
            miniApp: bool
          });
        }

        setTempNotifications() {
         // notifications state (temp) 
          const { searchNotifications } = this.props;
          this.setState({
            notif: searchNotifications
          });
        }

        handleFilterIcon(){
        // filter icon display
          this.setState({
            value: "",
            val: "",
            fNotif: [],
            temp: this.state.notif
          })
        }
        
        handleClearFilter(){
        // clear filter 
          this.setState({
            val: "",
            fNotif: [],
            temp: this.state.notif
          })
        }
        
        handleClearText(){
        // clear searchbar input
          this.setState({
            value: "",
            temp: this.state.notif
          })
        }

        setFlat(bool){
        // flatlist display
          this.setState({
            flat: bool,
          })
        }

        setValue(name) {
          // filter logic implementation
                const app = localizedAppName(name) + ' ';
                const newData = _.filter(this.state.notif, (item) => { 
                const appName = localizedAppName(item.subApplicationId);
                const itemTitle = appName ? appName.toUpperCase() : ''.toUpperCase();
                const text = localizedAppName(name);
                const textParts = text.toUpperCase().split(' ');
              let shouldInclude = true;
              for(let i =0; i < textParts.length; i++) {
                const part = textParts[i];
                shouldInclude =  shouldInclude && (itemTitle.indexOf(part)  > -1) ;
                if (!shouldInclude) {
                  return false;
                }
              }
              return true;
            });  
              this.setState({
                  val: app,
                  temp: newData,
                  fNotif: newData
                });
              }


          handleSearch = (text) => {
            // search logic implementation
            if(this.state.fNotif.length === 0){
            const newData = _.filter(this.state.notif, (item) => { 
                  const itemData = item.alert ? item.alert.toUpperCase() : ''.toUpperCase();
                  const textParts = text.toUpperCase().split(' ') ;
            let shouldInclude = true;
              for(let i =0; i < textParts.length; i++) {
                const part = textParts[i];
                shouldInclude =  shouldInclude && ((itemData.indexOf(part)  > -1));
                if (!shouldInclude) {
                  return false;
                }
              }
              return true;
            }); 
              this.setState({
                  value: text,
                  temp: newData
                });   
              }
              else{
                const newData = _.filter(this.state.fNotif, (item) => { 
                  const itemData = item.alert ? item.alert.toUpperCase() : ''.toUpperCase();
                  const textParts = text.toUpperCase().split(' ') ;
            let shouldInclude = true;
              for(let i =0; i < textParts.length; i++) {
                const part = textParts[i];
                shouldInclude =  shouldInclude && ((itemData.indexOf(part)  > -1));
                if (!shouldInclude) {
                  return false;
                }
              }
              return true;
            }); 
              this.setState({
                  value: text,
                  temp: newData
                });   
              }   
                }
    
          goToDetails(item) {
            // item detail
                return this.props.goToDetailFn(item);
              }

          setNotifRead(bool){
            // unread notification state
            return this.props.setNotifReadFn(bool);
          }

          setReadNotifCount(id){
            // Unread notification local list
            return this.props.readNotifCountFn(id);
          }
          
          update = async(item) => {
          // update "Status" to "Read"
            const userId = item.userId;
             if(item.status === 'New' && userId !== undefined){
             await updateNotifications(item.id);   // update API call
             } if (userId === undefined){
              console.log('General Message!!')
              return;
             }else {
               console.log('already read!!')
               return;
             }
          }

          renderItem({item}) {
            // FlatList item component 
              const {list, notificationRead} = this.props;
              return <SearchItem item={item}
                            onPress={() => {this.goToDetails(item);this.setNotifRead(true);this.update(item)}} 
                            notificationRead={notificationRead}
                            list={list}
                            value={this.state.value} 
                            readNotifCountFn={(id) => {this.setReadNotifCount(id)}}   /> ;
            }
    
 
    
   

          renderSearchBar(){
            // search bar - textinput with search/filter logic
            const { dropList } = this.props;
            const {container1, section} = styles;
            const {miniApp, value, val} = this.state;
        
              return (
                  <View>           
                  <View style={container1}>
                  <View style={section}>
                  <Icon name='search' size={20} />
                  { val === "" ? null :
                  <Label val={this.state.val}
                  accessible={true}
                  testID='filterLabel' 
                  accessibilityLabel="filterLabelOption"> {val} </Label>
                  }
                  <TextInput  
                    value={value} 
                    placeholder='Search Here...'
                    accessible={true}
                    testID='textInput' 
                    accessibilityLabel="textInputBar"
                    onChangeText={(text)=>{this.handleSearch(text);this.setFlat(true);this.renderDropDown(false)}}  
                    onTouchStart={()=>{this.setTempNotifications();this.renderDropDown(true)}}
                    style={{ height: 40, flex: 1}}
                    autoCapitalize='none'
                    selectTextOnFocus={true}
                    onKeyPress={({ nativeEvent }) => {
                      if (nativeEvent.key === 'Backspace' && value !== "") {
                        this.handleClearText();
                      }     
                      if (nativeEvent.key === 'Backspace' && value === "") {
                        this.handleClearFilter();
                      }             
                    }}
                  />
                  <Icon name='cancel' 
                  size={20}
                  accessible={true}
                  testID='clearIcon' 
                  accessibilityLabel="clearTextIcon"
                  onPress={()=>{this.handleFilterIcon();this.renderDropDown(true)}} /> 
                  <Button title='Cancel' 
                  buttonStyle={{backgroundColor:'#D3D3D3', borderColor: 'grey', borderRadius: 6 , borderWidth: 1, height: 40}}
                  onPress={()=>{this.setDisplaySearch(false);this.setIcon(true)}} 
                  accessible={true}
                  testID='cancelButton' 
                  accessibilityLabel="cancelSearchButton"/> 
                    </View>
                </View>
                  <View>
                  {
                    miniApp === true ?
                  <View>
                      {  
                        dropList.map((l,i) => (
                            <View key={l}>
                              <TouchableHighlight
                              onPress={()=>{this.setValue(l); this.setFlat(true)}}
                              underlayColor="blue" 
                              accessible={true}
                              testID='filterListTouch' 
                              accessibilityLabel="filterListClick">
                              <View>
                              <ListItem 
                                containerStyle={{padding: 8, backgroundColor:'#E6E6E6'}}
                                title={localizedAppName(l)}
                                leftIcon={ <Image style={{width:25, height: 25}} source={appImageIconrequireString(l)}  />    }  
                                bottomDivider
                                accessible={true}
                                testID='filterList' 
                                accessibilityLabel="filterListOption"
                                />
                              </View>
                              </TouchableHighlight>           
                          </View>
                        ))
                      }  
                      <View style={{padding: 9}}></View>
                  </View>
                  : null
                  }
                  </View>
                  
                  </View>
              );

          }

          renderFlatlistContent(){
            // notification list display
            const { temp, value, val} = this.state;
            vKey = ++vKey;
            let key = vKey.toString();

            return (
                <View style={{flex: 1}}  key={key}>
                  <View style={{padding: 3}}></View>
                  { 
                      val === "" && value === "" ?  null :
                      <View><Text style={{fontWeight: 'bold', fontSize:17}}> {localizedsearchResults()}  </Text>
                      </View>
                  }
                      <View style={{padding: 5}}></View>
                      {
                        temp.length === 0 ? <Text style={{fontWeight: 'bold', fontSize:20, textAlign:'center'}}> {localizednoMessages()} </Text> :
                      <View style={{flex: 1}}>
                      <FlatList
                        ListFooterComponent={<View style={{ paddingBottom: 30 }}></View>}
                        data={temp}
                        keyExtractor={(item) => item.id}
                        renderItem={this.renderItem.bind(this)}                        
                        />
                        </View>
                      }
                  </View>
                    
             
            )
          }


              render() {
                return (
                  <View style={{flex: 1}}>
                    { this.renderSearchBar() }
                    { this.renderFlatlistContent() }
                  </View>
                );
              }
          }

const styles = StyleSheet.create({
    container1: {
      justifyContent: 'center',
      alignItems: 'center',
      
    },
    section: {
      flexDirection: 'row',
      alignItems: 'center',
      backgroundColor: '#fff',
      borderWidth: 0.5,
      borderColor: '#000',
      height: 40,
      borderRadius: 5,
    },
  })
