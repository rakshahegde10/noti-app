import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';


export default function Label(props) {
    // Search Filter Label 
        return (
        <View style={{flexDirection:'row'}}>
        <Text style={styles.label}> {props.val} </Text>
        <View style={{padding: 3}}></View>
        </View>
        )
}

const styles = StyleSheet.create({
    label:{
      borderWidth: 1,
      backgroundColor: '#DCDCDC',
      left:2,
      borderRadius: 6,
      fontSize: 16
     }
  })