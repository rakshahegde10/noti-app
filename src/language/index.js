import I18n from "react-native-i18n";
import en from "./en.js";
import zh_hk from "./zh-hk.js";


I18n.fallbacks = true;

I18n.translations = {
    en,
    zh_hk
};

I18n.locale = "en";

export default I18n;
