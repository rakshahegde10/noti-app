export default {
    "title": "Notification Centre",
    "subTitle" : "My Notifications",
    "searchResults" : "Search Results",
    "noMessages" : "No Messages",
    "app_icon_name": {
        hago: "Hospital Authority",
        appointment: "My Appointments",
        bookha: "BookHA",
        payha: "Pay HA",
        rehab: "Rehab",
        medication: "Medication",
        easyq: "EasyQ",
        haconnect: "HA Connect",
        telecare: "Hospital Authority"
    },
    "messageCreateDate": {
        prefixString: "Sent: ",
        dateTimeFormatAM: "DD-MMM-YYYY hh:mm A",
        dateTimeFormatPM: "DD-MMM-YYYY hh:mm A"
    }
}