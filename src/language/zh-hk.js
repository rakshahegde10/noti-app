export default {
    "title": "訊息中心",
    "subTitle" : "我的訊息",
    "searchResults" : "搜尋結果",
    "noMessages" : "沒有搜尋結果",
    "app_icon_name": {
        hago: "醫院管理局",
        appointment: "預約紀錄",
        bookha: "預約通",
        payha: "繳費服務",
        rehab: "康復",
        easyq: "排隊易",
        medication: "藥物",
        haconnect: "醫管局與你",
        telecare: "醫院管理局"
    },
    "messageCreateDate": {
        prefixString: "發送於 ",
        dateTimeFormatAM: "YYYY年MM月DD日 上午hh:mm",
        dateTimeFormatPM: "YYYY年MM月DD日 下午hh:mm"
    }
}