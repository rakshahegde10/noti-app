import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    ScrollView,
    Dimensions,
    Image,
} from "react-native";

import { HAGo_getLanguage, HAGo_getFontSize } from '../HAGo/HAGo_authentication';
import I18n from '../language';

export default class HAGo_library_faq extends React.Component {
    static navigationOptions = ({ navigation }) => {
        const params = navigation.state.params || {};

        return {
            title: I18n.t('faq.title'),
            headerRight:<View></View> 
        }
    }

    constructor(props) {
        super(props);

        this.state = {
            equipWidth: Dimensions.get('window').width,
            equipHeight: Dimensions.get('window').height,
            loading: false,
            preRatio: HAGo_getFontSize() == 'small'?0.8:HAGo_getFontSize() == 'normal'?1:1.2,

            item1State:false,

        },

        this.onLayout = this.onLayout.bind(this);

    }

    onLayout(e) {
        this.setState({
            equipWidth: Dimensions.get('window').width,
            equipHeight: Dimensions.get('window').height
        })
        

    }

    normalizeFontSize = (fontsize) => {
        let fontScale;
        if (HAGo_getFontSize() == 'small'){
            fontScale = 0.8;
        }
        if (HAGo_getFontSize() == 'normal'){
            fontScale = 1;
        }
        if (HAGo_getFontSize() == 'large'){
            fontScale = 1.2;
        }
        
        return Math.round(fontsize*fontScale);
    }

    setAndroidLineHeight = (fontSize) => {
        //const multiplier = (fontSize > 20) ? 1.5 : 1;
        const multiplier = 1.4;
        return parseInt( (fontSize * multiplier), 10);
    }

    showDetail = (pages) => {
        this.setState({
            
            item1State: pages=="item1"? !this.state.item1State: false,

        });
    }

    render() {
      
        let that = this;
        return (
            <View onLayout={this.onLayout}
                      style={styles.container}>{this.state.loading?(<Loading size="large" color="#00f" />):null }
                <ScrollView style = {{flex:1,backgroundColor:"rgb(255,255,255)"}}>
       
                        <View style={styles.container}>
                                    
                            {
                                // Simple question 
                            }
                            <View>
                                <TouchableOpacity onPress={()=>{
                                        this.props.navigation.navigate('HAGoFAQPageAnswer',{
                                            question:I18n.t("faq.question1"),
                                            answer:I18n.t("faq.answer1")
                                        })
                                }} >
                                    <View style={[styles.item,{width:this.state.equipWidth,height:Math.round(80*this.state.preRatio)}]}>
                                        <Text style={[styles.itemText,{fontSize:this.normalizeFontSize(16)}]}>{I18n.t("faq.question1")}</Text>
                                    </View> 
                                </TouchableOpacity>
                            </View>

                            

                            {
                                // With category
                            }
                            <TouchableOpacity onPress={()=>{this.showDetail("item1")}}>
                                <View style={[styles.item,{width:this.state.equipWidth}]}>
                                    <Text style={[styles.itemText,{fontSize:this.normalizeFontSize(16)}]} >
                                        {I18n.t("faq.item1.title")} 
                                    </Text>

                                    {this.state.item1State?
                                        <Image  style={styles.itemImageAfterClick} source={require('./images/faq/more_nextAfterClick.png')} /> 
                                    :
                                        <View style={{flexDirection:'row'}} >
                                            <Image  style={styles.itemImage} source={require('./images/faq/more_next.png')} />
                                        </View>
                                    }
                                </View>
                            </TouchableOpacity>

                            {this.state.item1State?
                                <View>
                                    {
                                        // With link
                                    }
                                    <TouchableOpacity onPress={()=>{ 
                                        this.props.navigation.navigate('HAGoFAQPageAnswer',{
                                            question:I18n.t("faq.item1.question1"),
                                            answer:I18n.t("faq.item1.answer1_1") + "|" + I18n.t("faq.item1.answer1_2") + "|" + I18n.t("faq.item1.answer1_3"),
                                            url: 'https://www3.ha.org.hk/hago/Home/Patient/ActivateMyHaGo',
                                            type: "url"
                                        })
                                    }} >
                                    <View style={[styles.item,{paddingLeft:54,width:this.state.equipWidth,height:Math.round(80*this.state.preRatio),backgroundColor:'rgb(243,243,243)'}]}>
                                        <Text style={[styles.itemText,{fontSize:this.normalizeFontSize(16)}]}>{I18n.t("faq.item1.question1")}</Text>
                                    </View> 
                                    </TouchableOpacity>


                                    {
                                        // Simple question
                                    }
                                    <TouchableOpacity onPress={()=>{
                                        this.props.navigation.navigate('HAGoFAQPageAnswer',{
                                            question:I18n.t("faq.item1.question2"),
                                            answer:I18n.t("faq.item1.answer2")
                                        })
                                    }} >
                                        <View style={[styles.item,{paddingLeft:54,width:this.state.equipWidth,height:Math.round(80*this.state.preRatio),backgroundColor:'rgb(243,243,243)'}]}>
                                            <Text style={[styles.itemText,{fontSize:this.normalizeFontSize(16)}]}>{I18n.t("faq.item1.question2")}</Text>
                                        </View> 
                                    </TouchableOpacity>

                                </View>
                            :<View></View>
                            }

                            

                        </View>
                </ScrollView>
            </View>
        )
    }


}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "rgb(255,255,255)"
    },
    itemImage: {
        width: 9,
        height: 15
    },
    itemImageAfterClick: {
        width: 15,
        height: 9
    },
    switchBtn: {
        //right: 5,
        top: 0,
        width: 43,
        height: 26,

        alignItems: 'center',
        justifyContent: 'center'
    },
    example: {
        width: 200,
        height: 200,
        backgroundColor: "yellow",
        transform: [{ scale: 0.5 }, { translate: [100, 100] }]
    },

    item: {
        borderBottomWidth: 1,
        borderBottomColor: "rgba(85,85,85,0.15)",
        paddingLeft: 24,
        paddingRight: 20,
        justifyContent: "space-between",
        alignItems: 'center',
        height: 51,
        flexDirection: 'row'
    },
    itemText: {
        fontSize: 16,
        color: "rgb(0,0,0)"
    }
})

