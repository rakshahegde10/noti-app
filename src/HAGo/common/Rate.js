import { View, Text, StyleSheet, Platform, TouchableOpacity, Image, TouchableWithoutFeedback } from "react-native";
import React from 'react';
import PropTypes from 'prop-types';

const imgRate1 = require("../images/feedback/img_emoji_rate_1.png");
const imgRate2 = require("../images/feedback/img_emoji_rate_2.png");
const imgRate3 = require("../images/feedback/img_emoji_rate_3.png");
const imgRate4 = require("../images/feedback/img_emoji_rate_4.png");
const imgRate5 = require("../images/feedback/img_emoji_rate_5.png");
const imgStarSelected = require("../images/feedback/img_star_select.png");
const imgStarUnselected = require("../images/feedback/img_star_unselect.png");

//import { isTablet } from '../helpers/DimensionUtils';

export default class Rate extends React.Component {
    constructor(props){
        super(props);
        this._setRate = this._setRate.bind(this);
        this.renderStars = this.renderStars.bind(this);
        this.renderEmoji = this.renderEmoji.bind(this);
    }

    _setRate(index){
        this.props.setRate(index+1);
    }

    renderStars(){
        return Array(5).fill().map((item,index)=>{
            return (
                <TouchableWithoutFeedback onPress={()=>{this._setRate(index)} } key={index}>
                    <Image source={this.props.rate > index?imgStarSelected:imgStarUnselected} style={styles.imgStar} />
                </TouchableWithoutFeedback>
            )
        })
    }

    renderEmoji(){
        switch(this.props.rate){
            case 0:
                return null
            case 1:
                return (
                    <Image source={imgRate1} style={styles.imgStar} />
                )
            case 2:
                return (
                    <Image source={imgRate2} style={styles.imgStar} />
                )
            case 3:
                return (
                    <Image source={imgRate3} style={styles.imgStar} />
                )
            case 4:
                return (
                    <Image source={imgRate4} style={styles.imgStar} />
                )
            case 5:
                return (
                    <Image source={imgRate5} style={styles.imgStar} />
                )
            default:
                return null
        }
    }

    render() {
        return (
            <View style={styles.container} >
                {this.renderStars()}
                {this.renderEmoji()}
            </View>
        )
    }
}


const styles=StyleSheet.create({
    container: {
        flexDirection:"row",
        backgroundColor:"transparent",
        alignItems:"center",
    },
    imgStar:{
        width:45,
        height:45,
        marginRight:5,
    }
})

Rate.propTypes = {
    rate: PropTypes.number.isRequired,
    setRate: PropTypes.func.isRequired,
};

Rate.defaultProps = {
  rate:0,
  // setRate:(rate)=>{}
};