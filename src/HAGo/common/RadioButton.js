import { View, Text, StyleSheet, Platform, TouchableOpacity, Image, TouchableWithoutFeedback } from "react-native";
import React from 'react';
import PropTypes from 'prop-types';


export default class RadioButton extends React.Component {
    constructor(props){
        super(props);
    }

    render() {
        return (
            <TouchableOpacity onPress={this.props.onPress} >
                <View style={styles.outer}>
                    <View style={[styles.inner,{backgroundColor:this.props.checked?"#ccc":"white"}]}></View>
                </View>
            </TouchableOpacity>
        )
    }
}


const styles=StyleSheet.create({
    outer:{
        width:32,
        height:32,
        borderRadius:16,
        borderWidth:2,
        borderColor:"#ccc",
        backgroundColor:"white",
        alignItems:"center",
        justifyContent:"center",
    },
    inner:{
        width:16,
        height:16,
        borderRadius:8,
        backgroundColor:"#ccc"
    }
})

RadioButton.propTypes = {
    checked:PropTypes.bool,
    onPress:PropTypes.func,
};

RadioButton.defaultProps = {
    checked:false
  // setRate:(rate)=>{}
};