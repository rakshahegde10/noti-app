import React, { Component } from 'react';
import { NativeModules } from 'react-native';
import {PermissionsAndroid, ToastAndroid, Alert} from 'react-native';
import { HAGo_getLanguage } from '../HAGo_authentication';

class PermissionUtil {
	requestPermissionsFailMessage = (disabledPermission) =>{

        let title = '';
        let bodymessage = '';

        if (disabledPermission == 'storage,'){
            title = HAGo_getLanguage() == 'en'?'HA Go cannot access Device function':'HA Go未能使用你的電話功能';
            bodymessage = HAGo_getLanguage() == 'en'?'To enable the \'storage permissions\', go to \'Settings\' > \'HA Go\'.':'請在手機「設定」>「HA Go」允許「儲存裝置」功能。';
        } else if (disabledPermission == 'camera,'){
            title = HAGo_getLanguage() == 'en'?'HA Go cannot access Camera':'HA Go未能使用相機';
            bodymessage = HAGo_getLanguage() == 'en'?'To enable the camera function, go to \'Settings\' > \'HA Go\'.':'請在手機「設定」>「HA Go」允許相機功能。';
        } else if (disabledPermission == 'calendar,'){
            title = HAGo_getLanguage() == 'en'?'HA Go cannot access Calendar':'HA Go未能使用日曆';
            bodymessage = HAGo_getLanguage() == 'en'?'To enable the calendar function, go to \'Settings\' > \'HA Go\'.':'請在手機「設定」>「HA Go」允許日曆功能。';
        } else if (disabledPermission == 'location,'){
            title = HAGo_getLanguage() == 'en'?'HA Go cannot access Location':'HA Go未能使用位置功能';
            bodymessage = HAGo_getLanguage() == 'en'?'To enable the location function, go to \'Settings\' > \'HA Go\'.':'請在手機「設定」>「HA Go」允許位置資訊功能。';
        } else if (disabledPermission == 'microphone,'){
            title = HAGo_getLanguage() == 'en'?'HA Go cannot access Microphone':'HA Go未能使用麥克風功能';
            bodymessage = HAGo_getLanguage() == 'en'?'To enable the microphone function, go to \'Settings\' > \'HA Go\'.':'請在手機「設定」>「HA Go」允許麥克風功能。';
        } else {
            title = HAGo_getLanguage() == 'en'?'HA Go cannot access Device function':'HA Go未能使用你的電話功能';

            let multiple_permission_body_message = '';
            if (disabledPermission.includes('storage,')){
                multiple_permission_body_message += HAGo_getLanguage() == 'en'?'\'storage permissions\'':'「儲存裝置」' + HAGo_getLanguage() == 'en'?',':'、';
            }
            if (disabledPermission.includes('camera,')){
                multiple_permission_body_message += HAGo_getLanguage() == 'en'?'camera':'相機' + HAGo_getLanguage() == 'en'?',':'、';
            }
            if (disabledPermission.includes('calendar,')){
                multiple_permission_body_message += HAGo_getLanguage() == 'en'?'calendar':'日曆' + HAGo_getLanguage() == 'en'?',':'、';
            }
            if (disabledPermission.includes('sms,')){
                multiple_permission_body_message += HAGo_getLanguage() == 'en'?'SMS':'短訊' + HAGo_getLanguage() == 'en'?',':'、';
            }
            if (disabledPermission.includes('location,')){
                multiple_permission_body_message += HAGo_getLanguage() == 'en'?'Location':'位置資訊' + HAGo_getLanguage() == 'en'?',':'、';
            }
            if (disabledPermission.includes('microphone,')){
                multiple_permission_body_message += HAGo_getLanguage() == 'en'?'microphone':'麥克風' + HAGo_getLanguage() == 'en'?',':'、';
            }

            multiple_permission_body_message = multiple_permission_body_message.substring(0,multiple_permission_body_message.length-1);

            bodymessage = HAGo_getLanguage() == 'en'?'To enable the ':'請在手機「設定」>「HA Go」允許' + multiple_permission_body_message + HAGo_getLanguage() == 'en'?', go to \'Settings\' > \'HA Go\'.':'功能。';
        }


        Alert.alert(
              title,
              bodymessage,
              [{
                  text:HAGo_getLanguage() == 'en'?'Settings':'設定',
                  onPress:()=>{NativeModules.HAGoBridge.openSettingPage()},
              },{
                text:HAGo_getLanguage() == 'en'?'Cancel':'取消',
                onPress:()=>{},
                // style:'cancel'
              }],
              { cancelable: false }
        )
	}

	requestPermissions = async(permissions, enableNeverAskAgain) =>{
        try {
            const granted = await PermissionsAndroid.requestMultiple(permissions)
            //const granted = await PermissionsAndroid.requestMultiple(permissions).then((result) =>{Alert.alert("Result:"+JSON.stringify(result))})
            //ToastAndroid.show(JSON.stringify(granted), ToastAndroid.SHORT);
            let hasEnablePermission = false;
            let disabledPermission = '';
            for (let i=0;i<permissions.length;i++){
                hasEnablePermission = await PermissionsAndroid.check(permissions[i])
                if (!hasEnablePermission){
                	if (permissions[i] == PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE){
						disabledPermission += 'storage,';
                	}else if (permissions[i] == PermissionsAndroid.PERMISSIONS.CAMERA){
						disabledPermission += 'camera,';
                	//}else if (permissions[i] == PermissionsAndroid.PERMISSIONS.READ_CALENDAR){
                    //  disabledPermission += 'calendar,';
                    }else if (permissions[i] == PermissionsAndroid.PERMISSIONS.WRITE_CALENDAR){
                        disabledPermission += 'calendar,';
                    }else if (permissions[i] == PermissionsAndroid.PERMISSIONS.READ_SMS){
                        disabledPermission += 'sms,';
                    }else if (permissions[i] == PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION){
                        disabledPermission += 'location,';
                    }else if (permissions[i] == PermissionsAndroid.PERMISSIONS.RECORD_AUDIO){
                        disabledPermission += 'microphone,';
                    }
                }
            }

            if (disabledPermission == ''){
                hasEnablePermission = true;
            }else {
                hasEnablePermission = false;
            }

            //if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            if (!enableNeverAskAgain){
                return true
            }

            if (hasEnablePermission){    
              	return true
            } else if (JSON.stringify(granted).indexOf("never_ask_again") != -1){

                if (!disabledPermission.includes('sms,')){
                    let title = '';
                    let bodymessage = '';

                    if (disabledPermission == 'storage,'){
                        title = HAGo_getLanguage() == 'en'?'HA Go cannot access Device function':'HA Go未能使用你的電話功能';
                        bodymessage = HAGo_getLanguage() == 'en'?'To enable the \'storage permissions\', go to \'Settings\' > \'HA Go\'.':'請在手機「設定」>「HA Go」允許「儲存裝置」功能。';
                    } else if (disabledPermission == 'camera,'){
                        title = HAGo_getLanguage() == 'en'?'HA Go cannot access Camera':'HA Go未能使用相機';
                        bodymessage = HAGo_getLanguage() == 'en'?'To enable the camera function, go to \'Settings\' > \'HA Go\'.':'請在手機「設定」>「HA Go」允許相機功能。';
                    } else if (disabledPermission == 'calendar,'){
                        title = HAGo_getLanguage() == 'en'?'HA Go cannot access Calendar':'HA Go未能使用日曆';
                        bodymessage = HAGo_getLanguage() == 'en'?'To enable the calendar function, go to \'Settings\' > \'HA Go\'.':'請在手機「設定」>「HA Go」允許日曆功能。';
                    } else if (disabledPermission == 'location,'){
                        title = HAGo_getLanguage() == 'en'?'HA Go cannot access Location':'HA Go未能使用位置功能';
                        bodymessage = HAGo_getLanguage() == 'en'?'To enable the location function, go to \'Settings\' > \'HA Go\'.':'請在手機「設定」>「HA Go」允許位置資訊功能。';
                    } else if (disabledPermission == 'microphone,'){
                        title = HAGo_getLanguage() == 'en'?'HA Go cannot access Microphone':'HA Go未能使用麥克風功能';
                        bodymessage = HAGo_getLanguage() == 'en'?'To enable the microphone function, go to \'Settings\' > \'HA Go\'.':'請在手機「設定」>「HA Go」允許麥克風功能。';
                    } else {
                        title = HAGo_getLanguage() == 'en'?'HA Go cannot access Device function':'HA Go未能使用你的電話功能';

                        let multiple_permission_body_message = '';
                        if (disabledPermission.includes('storage,')){
                            multiple_permission_body_message += HAGo_getLanguage() == 'en'?'\'storage permissions\'':'「儲存裝置」' + HAGo_getLanguage() == 'en'?',':'、';
                        }
                        if (disabledPermission.includes('camera,')){
                            multiple_permission_body_message += HAGo_getLanguage() == 'en'?'camera':'相機' + HAGo_getLanguage() == 'en'?',':'、';
                        }
                        if (disabledPermission.includes('calendar,')){
                            multiple_permission_body_message += HAGo_getLanguage() == 'en'?'calendar':'日曆' + HAGo_getLanguage() == 'en'?',':'、';
                        }
                        if (disabledPermission.includes('location,')){
                            multiple_permission_body_message += HAGo_getLanguage() == 'en'?'Location':'位置資訊' + HAGo_getLanguage() == 'en'?',':'、';
                        }
                        if (disabledPermission.includes('microphone,')){
                            multiple_permission_body_message += HAGo_getLanguage() == 'en'?'microphone':'麥克風' + HAGo_getLanguage() == 'en'?',':'、';
                        }

                        multiple_permission_body_message = multiple_permission_body_message.substring(0,multiple_permission_body_message.length-1);

                        bodymessage = HAGo_getLanguage() == 'en'?'To enable the ':'請在手機「設定」>「HA Go」允許' + multiple_permission_body_message + HAGo_getLanguage() == 'en'?', go to \'Settings\' > \'HA Go\'.':'功能。';
                    }

                  	Alert.alert(
    		              title,
    		              bodymessage,
    		              [{
    		                text:HAGo_getLanguage() == 'en'?'Settings':'設定',
    		                onPress:()=>{NativeModules.HAGoBridge.openSettingPage()},
    		                // style:'cancel'
    		              },
    		              {
    		              	text:HAGo_getLanguage() == 'en'?'Cancel':'取消',
    		                onPress:()=>{},
    		              }],
    		              { cancelable: false }
    		            )
                }
              	
              	return false
            } else {
            	this.requestPermissionsFailMessage(disabledPermission)
            	return false
            }
        } catch (err) {
        }
        return false
    }
	
	requestTelecarePermissionsFailMessage = (disabledPermission) =>{

        let title = HAGo_getLanguage() == 'en'?'HA Go cannot proceed tele-consultation':'HA Go未能進行視像診症';
        let bodymessage = '';

        if (disabledPermission == 'camera,'){
            bodymessage = HAGo_getLanguage() == 'en'?'HA Go cannot access Camera':'HA Go未能使用相機';
        } else if (disabledPermission == 'microphone,'){
            bodymessage = HAGo_getLanguage() == 'en'?'To enable the microphone function, go to \'Settings\' > \'HA Go\'.':'請在手機「設定」>「HA Go」允許麥克風功能。';
        } else {
            if (disabledPermission.includes('camera,') || disabledPermission.includes('microphone,') ){
                bodymessage = HAGo_getLanguage() == 'en'?'Please tap \'Settings\' and turn on your microphone and camera of your mobile device.':'請按「設定」開啟流動裝置的相機及麥克風。';
            }
        }
        
        Alert.alert(
              title,
              bodymessage,
              [{
                text:HAGo_getLanguage() == 'en'?'Cancel':'取消',
                onPress:()=>{},
              },
              {
                text:HAGo_getLanguage() == 'en'?'Settings':'設定',
                onPress:()=>{NativeModules.HAGoBridge.openSettingPage()},
              }],
              { cancelable: false }
        )
    }

    requestTelecarePermissions = async(permissions, enableNeverAskAgain) =>{
        try {
            const granted = await PermissionsAndroid.requestMultiple(permissions)
            
            let hasEnablePermission = false;
            let disabledPermission = '';
            for (let i=0;i<permissions.length;i++){
                hasEnablePermission = await PermissionsAndroid.check(permissions[i])
                if (!hasEnablePermission){
                    if (permissions[i] == PermissionsAndroid.PERMISSIONS.CAMERA){
                        disabledPermission += 'camera,';
                    }else if (permissions[i] == PermissionsAndroid.PERMISSIONS.RECORD_AUDIO){
                        disabledPermission += 'microphone,';
                    }
                }
            }

            if (disabledPermission == ''){
                hasEnablePermission = true;
            }else {
                hasEnablePermission = false;
            }

            if (!enableNeverAskAgain){
                return true
            }

            if (hasEnablePermission){    
                return true
            } else if (JSON.stringify(granted).indexOf("never_ask_again") != -1){
                    let title = HAGo_getLanguage() == 'en'?'HA Go cannot proceed tele-consultation':'HA Go未能進行視像診症';
                    let bodymessage = '';

                    if (disabledPermission == 'camera,'){
                        bodymessage = HAGo_getLanguage() == 'en'?'HA Go cannot access Camera':'HA Go未能使用相機';
                    } else if (disabledPermission == 'microphone,'){
                        bodymessage = HAGo_getLanguage() == 'en'?'To enable the microphone function, go to \'Settings\' > \'HA Go\'.':'請在手機「設定」>「HA Go」允許麥克風功能。';
                    } else {
                        if (disabledPermission.includes('camera,') || disabledPermission.includes('microphone,') ){
                            bodymessage = HAGo_getLanguage() == 'en'?'Please tap \'Settings\' and turn on your microphone and camera of your mobile device.':'請按「設定」開啟流動裝置的相機及麥克風。';
                        }
                    }

                    Alert.alert(
                          title,
                          bodymessage,
                          [{
                            text:HAGo_getLanguage() == 'en'?'Cancel':'取消',
                            onPress:()=>{},
                          },
                          {
                            text:HAGo_getLanguage() == 'en'?'Settings':'設定',
                            onPress:()=>{NativeModules.HAGoBridge.openSettingPage()},
                          }],
                          { cancelable: false }
                    )
                
                return false
            } else {
                this.requestTelecarePermissionsFailMessage(disabledPermission)
                return false
            }
        } catch (err) {
        }
        return false
    }
}


var permissionUtil = new PermissionUtil();

module.exports = permissionUtil;