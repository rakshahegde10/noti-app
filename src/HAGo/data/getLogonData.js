﻿import React, {Component} from 'react';
import appJson from '../../../app.json';

export async function getLogonData() {
  let json = {}

  if (appJson.name == 'appointment'){
    json.username = 'carerapp01'
    json.password = 'hagopassword'
    json.carerid = '2020011014391847237805'
  } else if (appJson.name == 'HAPayment'){
    json.username = 'carerpay01'
    json.password = 'hagopassword'
    json.carerid = '2020011014464164485964'
  } else if (appJson.name == 'ha_rehab_reactnative'){
    json.username = 'carerreh01'
    json.password = 'hagopassword'
    json.carerid = '2020011014491196427556'
  } else if (appJson.name == 'ha_medication_reactnative'){
    json.username = 'carermed01'
    json.password = 'hagopassword'
    json.carerid = '2020010918592700523532'
  } else if (appJson.name == 'ha_allergy_reactnative'){
    json.username = 'carerall01'
    json.password = 'hagopassword'
    json.carerid = '2020052710011208001067'
  } else if (appJson.name == 'hago_easyq_app'){
    json.username = 'carereas01'
    json.password = 'hagopassword'
    json.carerid = '2020052710051379664104'
  } else if (appJson.name == 'gopc'){
    json.username = 'carergop01'
    json.password = 'hagopassword'
    json.carerid = '2020052710084775012488'
  } else if (appJson.name == 'HANotifApp'){
    json.username = 'carernot01'
    json.password = 'hagopassword'
    json.carerid = '2020052710131178777595'
  } else if (appJson.name == 'jrc'){
    json.username = 'carerjrc01'
    json.password = 'hagopassword'
    json.carerid = '2020110515294382740526'
  } else if (appJson.name == 'ha_myhealth_reactnative'){
    json.username = 'carermyh01'
    json.password = 'hagopassword'
    json.carerid = '2021020915524072465518'
  } else if (appJson.name == 'ha_myrecord_reactnative'){
    json.username = 'carermyr01'
    json.password = 'hagopassword'
    json.carerid = '2021020915570122389783'
  } else {
    json.username = 'carerapp01'
    json.password = 'hagopassword'
    json.carerid = '2020011014391847237805'
  }
  
  return json
}


