import React from 'react';
import {
    View,
    Text,
    Platform,
    ScrollView,
	TextInput,
	StyleSheet,
    TouchableOpacity,
    Alert,
	Dimensions,
} from "react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import { getFeedbackQuestion, submitFeedBack, HAGo_getLanguage, HAGo_getFontSize } from './HAGo_authentication';

import Rate from "./common/Rate.js";
import RadioButton from "./common/RadioButton.js";
import Base64 from "./common/base64.js"; 
import Loading from './common/PatientLoading';
import appJson  from '../../app.json';

const isAndroid = Platform.OS === 'android';
const config = {
        validation: {
            emailRegx: /(?:[a-z0-9!#$%&''*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&''*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/,
        },
};

export default class HAGo_library_feedback extends React.Component {

    constructor(props) {
        super(props);

        this.props.navigation.setOptions({ 
            title: HAGo_getLanguage() == 'en'?'Feedback':'意見調查',
            headerRight:(props) => (<View></View>),
            headerTitleStyle: {
                fontWeight: 'bold',
                textAlign: 'center',
                flex: 1,
                equipWidth:Dimensions.get('window').width,
                equipHeight:Dimensions.get('window').height,
                submitButtonWidth:Dimensions.get('window').width-30,
            },
        })

        this.state = {
            equipWidth: Dimensions.get('window').width,
            equipHeight: Dimensions.get('window').height,
            loading: false,
            questions: [],
            answers: {},
            email:'',
            name:'',
            submited:false,
            survey_version:'',
            // React Navigation 5.x: this.props.route.params
            // React Navigation 4.X: this.props.navigation.state.params
            app_id:this.props.route.params.feedback_id,
            is_show_name:this.props.route.params.is_show_name=='N'?'N':'Y',
            is_show_email:this.props.route.params.is_show_email=='N'?'N':'Y',
        },

        this.onLayout = this.onLayout.bind(this);
    }

    normalizeFontSize = (fontsize) => {
        let fontScale;
        if (HAGo_getFontSize() == 'small'){
            fontScale = 0.8;
        }
        if (HAGo_getFontSize() == 'normal'){
            fontScale = 1;
        }
        if (HAGo_getFontSize() == 'large'){
            fontScale = 1.2;
        }
        
        return Math.round(fontsize*fontScale);
    }

    setAndroidLineHeight = (fontSize) => {
        //const multiplier = (fontSize > 20) ? 1.5 : 1;
        const multiplier = 1.4;
        return parseInt( (fontSize * multiplier), 10);
    }

    componentDidMount(){
        this.setState({loading:true})
		
		let input = {}
		input.app_id = this.state.app_id

        getFeedbackQuestion(input).then(data =>{
            let questions= data.questions;
            questions.sort((a,b)=>{return a.questionOrder - b.questionOrder>0?1:-1});

            this.setState({
              questions: questions,
              loading:false,
              survey_version:questions[0].surveyVersion.toString(),
            })
            
        }).catch(err=> {
            this.setState({loading:false})
        })
    }

    submit(){
        this.setState({
          submited: true
        });
        //找出mandatory 问题
        let mandatoryQuestionList = this.state.questions.filter((question) =>{
          if(question.isMandatory === "1"){
            return question
          }
        });
        let unFilledAnswerList = mandatoryQuestionList.map((question)=>{
          let key = question.questionOrder;
          let answer = this.state.answers[key];
          if(!answer){
            return 1
          }
        }).filter((answer)=> !!answer)
        if(unFilledAnswerList.length >0){
          return;
        }
        
        let length = Object.keys(this.state.answers).length;
        if(length === 0){
          Alert.alert(null,HAGo_getLanguage() == 'en'?'Please input feedback':'請提交意見調查',[
            {text:HAGo_getLanguage() == 'en'?'OK':'確定',onPressL:()=>{}}
          ])
          return;
        }
        let data = {};
        let feedback = "";
        Object.keys(this.state.answers).forEach((key,index)=>{
          let answer = this.state.answers[key];
          let question = this.state.questions.find((question) => question.questionOrder == key);
          //base64 input text
          if(question.question_type === "FREE"){
            answer = Base64.btoa(answer);
          }
          if(index === 0){
            feedback += `${key}=${answer}`
          }else{
            feedback += `|${key}=${answer}`
          }
        })

        // 20190911 Add check email workflow
        if(this.state.email !="") { 
          if(!config.validation.emailRegx.test(this.state.email)){
            Alert.alert(HAGo_getLanguage() == 'en'?'Invalid Input':'輸入錯誤',HAGo_getLanguage() == 'en'?'Please input valid email address':'請輸入正確的電郵地址',[{text:HAGo_getLanguage() == 'en'?'OK':'確定',onPress:()=>{}}],{ cancelable: false })
            return;
          }
        }

        data.feedback = feedback;
        data.email = this.state.email;
        data.name = this.state.name;
        data.app_id = this.state.app_id;
        data.survey_version = this.state.survey_version;
        this.setState({loading:true});

        let { hagofeedbackCallBack } = this.props.route.params

        submitFeedBack(data).then(result =>{
          this.setState({loading:false})
          Alert.alert(null,HAGo_getLanguage() == 'en'?'Thank you for your feedback!':'謝謝你的意見！',[
            {text: HAGo_getLanguage() == 'en'?'OK':'確定', onPress: () => {
                if (hagofeedbackCallBack && typeof hagofeedbackCallBack === "function"){
                  hagofeedbackCallBack("Y")
                }
                this.props.navigation.goBack() 
            }},
          ],{cancelable: false})
          
        }).catch(err =>{
          if (hagofeedbackCallBack && typeof hagofeedbackCallBack === "function"){
                hagofeedbackCallBack("N")
          }
          this.setState({loading:false})
        })
    }

    renderRateItem(question){
      let key = question.questionOrder;
      let answer = this.state.answers[key];
      return (
        <View style={styles.questionItem} key={question.questionOrder}>
          <Text style={[styles.questionText,{fontSize:this.normalizeFontSize(16)},Platform.OS === 'android' && HAGo_getLanguage()!='en' && Platform.Version >= 28?{lineHeight:this.setAndroidLineHeight(this.normalizeFontSize(16))}:'']} >
            {question.questionOrder}.
            {" "}
            <Text>{question.questionContent}</Text>
            {question.isMandatory === "0" && <Text>{" "}({HAGo_getLanguage() == 'en'?'Optional':'選擇性填寫'})</Text>}
          </Text>
          <Rate rate={answer} setRate={this.setAnswer.bind(this,key) } />
          {this.state.submited && question.isMandatory === "1" && !answer && <Text style={styles.error}>{HAGo_getLanguage() == 'en'?'This is mandatory':'必須填寫'}</Text>}
        </View>
      )
    }

    scrollToTextInput=(this_Y,pageY)=>{
        if(isAndroid){return;}
        var view_height = 130;
        var scroll_value = 0;

        scroll_value = this_Y
        this._keyboardAwareScrollView.scrollTo({y: scroll_value})
    }

    renderInputItem(question){
      let key = question.questionOrder;
      let answer = this.state.answers[key];
      return (
        <View style={styles.questionItem} key={question.questionOrder} ref={(ref) => { this[`textInput${question.questionOrder}`] = ref }}>
          <Text style={[styles.questionText,{fontSize:this.normalizeFontSize(16)},Platform.OS === 'android' && HAGo_getLanguage()!='en' && Platform.Version >= 28?{lineHeight:this.setAndroidLineHeight(this.normalizeFontSize(16))}:'']}>
            {question.questionOrder}.
              {" "}
              <Text style={{fontSize:this.normalizeFontSize(16)}}>
                {question.questionContent}
                {question.isMandatory === "0" && <Text>{" "}({HAGo_getLanguage() == 'en'?'Optional':'選擇性填寫'})</Text>}
              </Text>
          </Text>
          <TextInput 
              
              onFocus={(event: any) => {
                              if (this[`textInput${question.questionOrder}`]) {
                                          this[`textInput${question.questionOrder}`].measure((x, y, width, height, pageX, pageY) => {
                                          this.scrollToTextInput(y, pageY)
                                      })
                              }
                            }}
              value={answer}
              onChangeText={this.setAnswer.bind(this,key)}
              style={[styles.textarea,{fontSize:this.normalizeFontSize(16),textAlignVertical:"top"}]} 
              multiline={true} 
              numberOfLines = {4}
              maxLength={question.questionType=="FREE"?1000:3000}
          />
          {this.state.submited && question.isMandatory === "1" && !answer && <Text style={styles.error}>{HAGo_getLanguage() == 'en'?'This is mandatory':'必須填寫'}</Text>}
        </View>
      )
    }

    setAnswer(key,answer){
      this.setState({
        answers:Object.assign({},this.state.answers,{[key]:answer})
      })
    }

    renderRadioItem(question){
      let questionContent = question.questionContent;
      let splitIndex = questionContent.indexOf("|");
      let questionPart = questionContent.substr(0,splitIndex);
      let answerPart = questionContent.substr(splitIndex+1);
      let answers = answerPart.split("|");
      let key = question.questionOrder;
      let answer = this.state.answers[key];
      
      return (
        <View style={styles.questionItem} key={question.questionOrder}>
          <Text style={[styles.questionText,{fontSize:this.normalizeFontSize(16)},Platform.OS === 'android' && HAGo_getLanguage()!='en' && Platform.Version >= 28?{lineHeight:this.setAndroidLineHeight(this.normalizeFontSize(16))}:'']} >
            {question.questionOrder}.
            {" "}
            <Text>{questionPart}</Text>
            {question.isMandatory === "0" && <Text>{" "}({HAGo_getLanguage() == 'en'?'Optional':'選擇性填寫'})</Text>}
          </Text>
          {answers.map((item,index)=>{
            return (
              <View style={styles.radioItem} key={index} >
                <RadioButton checked={item === this.state.answers[key]} onPress={this.setAnswer.bind(this,key,item)} />
                <Text style={[styles.radioLabel,{fontSize:this.normalizeFontSize(16)}]}>{item}</Text>
              </View>
            )
          })}
          {this.state.submited && question.isMandatory === "1" && !answer && <Text style={styles.error}>{HAGo_getLanguage() == 'en'?'This is mandatory':'必須填寫'}</Text>}
        </View>
      )
    }

    _renderItem(question){
      switch(question.questionType){
        case "RADIO":
          return this.renderRadioItem(question);
        case "FREE":
        case "LONGFREE":
          return this.renderInputItem(question);
        case "RATE":
          return this.renderRateItem(question);
        default:            
          break;
      }
    }

    setName(value){
      this.setState({name:value})
    }

    setEmail(value){
      this.setState({email:value})
    }


    onLayout(e) {
        this.setState({
            equipWidth: Dimensions.get('window').width,
            equipHeight: Dimensions.get('window').height,
            submitButtonWidth:Dimensions.get('window').width-30
        })
        

    }

    render(){
        return (
          <View style={styles.container} onLayout={this.onLayout.bind(this)}>
            {this.state.loading?(<Loading size="large" color="#00f" />):null}
            {!this.state.loading && <KeyboardAwareScrollView
              ref={(keyboardAwareScrollView) => { this._keyboardAwareScrollView = keyboardAwareScrollView; }}
              style={styles.scrollview}
              contentContainerStyle={styles.contentContainer}
            > 
              {this.state.questions.map((question,index) =>{
                return this._renderItem(question);
              })}
              {this.state.is_show_name == 'N'?null:
              (<View style={styles.questionItem} >
                <Text style={[styles.questionText,{fontSize:this.normalizeFontSize(16)}]} >{this.state.questions.length+1}.{" "}<Text>{HAGo_getLanguage() == 'en'?'Name':'姓名'} ({HAGo_getLanguage() == 'en'?'Optional':'選擇性填寫'})</Text></Text>
                <TextInput 
                    value={this.state.name}
                    onChangeText={this.setName.bind(this)}
                    style={[styles.textInput,{fontSize:this.normalizeFontSize(16)}]} 
                    numberOfLines = {1}
                    maxLength={48}
                />
              </View>)
			  }
			  {this.state.is_show_email == 'N'?null:
			  (<View style={styles.questionItem} >
                <Text style={[styles.questionText,{fontSize:this.normalizeFontSize(16)}]} >{this.state.is_show_name == 'N'?this.state.questions.length+1:this.state.questions.length+2}.{" "}<Text>{HAGo_getLanguage() == 'en'?'Email Address':'電郵地址'} ({HAGo_getLanguage() == 'en'?'Optional':'選擇性填寫'})</Text></Text>
                <TextInput 
                    value={this.state.email}
                    onChangeText={this.setEmail.bind(this)}
                    style={[styles.textInput,{fontSize:this.normalizeFontSize(16)}]} 
                    numberOfLines = {1}
                    maxLength={255}
                />
              </View>)
			  }
              <TouchableOpacity
                onPress={this.submit.bind(this)}
                style={[styles.loginButton,{width:this.state.submitButtonWidth,backgroundColor:this.state.disabled?'gray':'rgb(238,0,8)'}]}
              >

                  <Text style={[styles.btText,{fontSize:this.normalizeFontSize(16)}]}>{HAGo_getLanguage() == 'en'?'Submit':'提交'}</Text>
                </TouchableOpacity>
            </KeyboardAwareScrollView>
          }
          </View>
        );
    }



}



const styles = StyleSheet.create({
    error:{
        fontSize:14,
        color:"red",
        marginTop:3
    },
    container:{
        //borderWidth:1,
        //borderColor:"blue",
        flex:1,
        backgroundColor:"#fff",
    },
    scrollview:{
        //borderWidth:1,
        //borderColor:"green",
        paddingHorizontal:15,
        paddingTop:20,
    },
    contentContainer:{
        //borderWidth:1,
        //borderColor:"black",
        paddingBottom:45
    },
    questionItem:{
        //borderWidth:1,
        //borderColor:"red",
        backgroundColor:"#fff",
        marginBottom:25,
    },
    questionText:{
        marginBottom:8,
    },
    radioItem:{
        flexDirection:"row",
        alignItems:"center",
        paddingLeft:20,
        marginVertical:5,
    },
    radioLabel:{
        marginLeft:25,
    },
    textarea:{
        borderWidth:1,
        borderColor:"#ccc",
        height:100,
        padding:10,
        borderRadius:5,
    },
    textInput:{
        borderWidth:1,
        borderColor:"#ccc",
        borderRadius:3,
        paddingHorizontal:10,
        paddingVertical:3,
    },
    loginButton: {
        height: 40,
        // width: width - 30,
        borderRadius: 40,
        backgroundColor: 'rgb(238,0,8)',
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 2,
        marginTop: 30,
      alignSelf:"center"
    },
    btText: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold',
    },
})

