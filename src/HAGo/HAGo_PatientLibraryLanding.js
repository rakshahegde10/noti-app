import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View , Dimensions, TouchableOpacity, AsyncStorage, DeviceEventEmitter} from 'react-native';
import I18n from '../language';
import {HAGo_init, HAGo_patient_library, HAGo_getInfo, HAGo_getLanguage, HAGo_getFontSize, HAGo_getNativeStorage} from "./HAGo_authentication";
import Loading from './common/PatientLoading';
import {fetchUnreadFlag} from '../home/NotificationAPI';
import {Badge} from 'react-native-elements';

const {width, height} = Dimensions.get('window');

type Props = {};
export default class HAGo_PatientLibraryLanding extends Component<Props> {

    constructor(props){
        super(props);      
        this.state={
            equipWidth:Dimensions.get('window').width,
            loading: false,
            unreadCount: ""
        }
        this.onLayout=this.onLayout.bind(this);
    }

    onLayout(e){
        this.setState({
            equipWidth:Dimensions.get('window').width
        })
    }

    async componentDidMount() {
        this.props.navigation.setOptions({ headerTitle: 'HA Go Library', headerTitleAlign: 'center' })
        this.loadingStatusListener = DeviceEventEmitter.addListener('HAGO_APP_LOADING_STATUS_CHANGE',(loading)=>{
            this.setState({loading:loading})
        });
        var app_id = 'notification'
        // ############################################################################### //
        // ############################################################################### //
        // ############################################################################### //
        var navigatePage = 'NotificationLanding'

        var language = 'en' //HAGo_getLanguage()
        var API_PATH = "https://hago-corp-gateway-sit.appdev.ha.org.hk:16845/hago/gw/2/"

        var info = HAGo_getInfo()


        let client_access_token = await AsyncStorage.getItem('client_access_token')
        let rsa_public_key = await AsyncStorage.getItem('rsa_public_key')

        //let fingerPrintStatus = await CheckFingerPrint()

        let user_status = await AsyncStorage.getItem("user_status")

        var user_id = null
        var user_access_token = null
        if (user_status == "CONFIRMED") {
            user_id = await AsyncStorage.getItem("user_id")
            user_access_token = await AsyncStorage.getItem('user_access_token')
        }
        console.log("user id:", user_id)
        let last_name = await AsyncStorage.getItem('last_name')
        let first_name = await AsyncStorage.getItem('first_name')
        let mobile_phone_no = await AsyncStorage.getItem('mobile_phone_no')

        let user_is_login = await AsyncStorage.getItem("user_is_login")
        
        let pushToken = await AsyncStorage.getItem('token')
        let device_id = (pushToken == null) ? "" :pushToken


        let getUndercareInfo = {
            "is_undercare_mode" : (await AsyncStorage.getItem('in_undercare_mode')) != null ? await AsyncStorage.getItem('in_undercare_mode') : 'false',
            "undercare_user_id" : await AsyncStorage.getItem('undercare_user_id'),
            "undercare_profile_pic" : await AsyncStorage.getItem('undercarer_pic'),
            "undercare_chinese_name" : await AsyncStorage.getItem('undercarer_chinese_name'),
            "undercare_nickname" : await AsyncStorage.getItem('undercarer_nickname'),
            "undercare_english_name" : await AsyncStorage.getItem('undercarer_english_name'),
            "undercare_color" : '#008A20',
            "undercare_user_role" : await AsyncStorage.getItem('undercare_user_role'),
        }

        let undercare_info = getUndercareInfo

        let user_device_info = {
            "device_uuid": info.device_info.device_uuid,
            "device_id": info.device_info.device_id,
            "device_name": info.device_info.device_name,
            "device_type": info.device_info.device_type,
            "device_language": language,
            "fingerprint_enabled": false,
            "bundle_id": info.device_info.bundle_id,
            "app_version": info.device_info.app_version
        }

        let hago_auth_info = {
            "client_access_token": client_access_token,
            "user_access_token": user_access_token,
            "rsa_public_key": rsa_public_key,
            "device_info": user_device_info,
            "request_credential": info.request_credential,
            "user_id": user_id,
            "user_is_login": user_is_login,
            "client_id": info.client_id,
            "client_secret": info.client_secret,
            "api_path": API_PATH,
            "user_status": user_status,
            "last_name": last_name,
            "first_name": first_name,
            "mobile_phone_no": mobile_phone_no,
            "undercare_info": undercare_info,
            "app_config": {
                "external_server_url": "https://gopc-app-gopc-corp-booking-dev-1.cldpaast71.server.ha.org.hk/hago/gopc",
                "hago-api-client-id": "hk.org.ha.hago.notification.cloud.test",
                "notification-retrieve-api": "https://fryujfr6q4.execute-api.ap-east-1.amazonaws.com/sit/v1/notification/retrieve",
                "notification-update-api" : "https://fryujfr6q4.execute-api.ap-east-1.amazonaws.com/sit/v1/notification/update",
                "cognito-domain": "https://cms-sit.auth.ap-southeast-1.amazoncognito.com/oauth2/token",
                "cognito-hago-client-id": "2d64vs1o644bl11b8d4kblh65p",
                "cognito-hago-client-secret": "fii0sskanhgnrq3mnaiukvqpqo78quia7ipm4lk7okio6k2upha",
                "aws_hago_applicationId": "hk.org.ha.testhago",
                "no_message_en": "You have no message.",
                "no_message_zh_hk": "您沒有訊息。",
                "no_service_en": "Service is temporarily unavailable, please try again later.",
                "no_service_zh_hk": "暫時未能提供服務，請稍後再試。"
            }
         }

         let HAGo_props = {
            "hago_auth_info": hago_auth_info,
            "language":language,
            "textsize":'normal',         //HAGo_getFontSize(),
            "app_id": app_id,
            "from_app_id": 'hago'
         }

         // Pass parameter to mini app
         HAGo_init(HAGo_props)
         I18n.locale = language;

         fetchUnreadFlag();
         HAGo_getNativeStorage('notificationApi_unreadCountFlag').then((data) => { this.setState({
            unreadCount: data,
                });
            }).catch((error) => {
            console.log('error')
            });

    }

    componentWillUnmount() {
        this.loadingStatusListener && this.loadingStatusListener.remove();
    }

    async pressNext(){
        // ############################################################################### //
        // ############################################################################### //
        // ######################### Please edit your app_id here ######################## //
        var app_id = 'notification'
        // ############################################################################### //
        // ############################################################################### //
        // ############################################################################### //
        var navigatePage = 'NotificationLanding'

        var language = 'en' //HAGo_getLanguage()
        var API_PATH = "https://hago-corp-gateway-sit.appdev.ha.org.hk:16845/hago/gw/2/"

        var info = HAGo_getInfo()


        let client_access_token = await AsyncStorage.getItem('client_access_token')
        let rsa_public_key = await AsyncStorage.getItem('rsa_public_key')

        //let fingerPrintStatus = await CheckFingerPrint()

        let user_status = await AsyncStorage.getItem("user_status")

        var user_id = null
        var user_access_token = null
        if (user_status == "CONFIRMED") {
            user_id = await AsyncStorage.getItem("user_id")
            user_access_token = await AsyncStorage.getItem('user_access_token')
        }
        console.log("user id:", user_id)
        let last_name = await AsyncStorage.getItem('last_name')
        let first_name = await AsyncStorage.getItem('first_name')
        let mobile_phone_no = await AsyncStorage.getItem('mobile_phone_no')

        let user_is_login = await AsyncStorage.getItem("user_is_login")
        
        let pushToken = await AsyncStorage.getItem('token')
        let device_id = (pushToken == null) ? "" :pushToken


        let getUndercareInfo = {
            "is_undercare_mode" : (await AsyncStorage.getItem('in_undercare_mode')) != null ? await AsyncStorage.getItem('in_undercare_mode') : 'false',
            "undercare_user_id" : await AsyncStorage.getItem('undercare_user_id'),
            "undercare_profile_pic" : await AsyncStorage.getItem('undercarer_pic'),
            "undercare_chinese_name" : await AsyncStorage.getItem('undercarer_chinese_name'),
            "undercare_nickname" : await AsyncStorage.getItem('undercarer_nickname'),
            "undercare_english_name" : await AsyncStorage.getItem('undercarer_english_name'),
            "undercare_color" : '#008A20',
            "undercare_user_role" : await AsyncStorage.getItem('undercare_user_role'),
        }

        let undercare_info = getUndercareInfo

        let user_device_info = {
            "device_uuid": info.device_info.device_uuid,
            "device_id": info.device_info.device_id,
            "device_name": info.device_info.device_name,
            "device_type": info.device_info.device_type,
            "device_language": language,
            "fingerprint_enabled": false,
            "bundle_id": info.device_info.bundle_id,
            "app_version": info.device_info.app_version
        }

        let hago_auth_info = {
            "client_access_token": client_access_token,
            "user_access_token": user_access_token,
            "rsa_public_key": rsa_public_key,
            "device_info": user_device_info,
            "request_credential": info.request_credential,
            "user_id": user_id,
            "user_is_login": user_is_login,
            "client_id": info.client_id,
            "client_secret": info.client_secret,
            "api_path": API_PATH,
            "user_status": user_status,
            "last_name": last_name,
            "first_name": first_name,
            "mobile_phone_no": mobile_phone_no,
            "undercare_info": undercare_info,
            "app_config": {
                "external_server_url": "https://gopc-app-gopc-corp-booking-dev-1.cldpaast71.server.ha.org.hk/hago/gopc",
                "hago-api-client-id": "hk.org.ha.hago.notification.cloud.test",
                "notification-retrieve-api": "https://fryujfr6q4.execute-api.ap-east-1.amazonaws.com/sit/v1/notification/retrieve",
                "notification-update-api" : "https://fryujfr6q4.execute-api.ap-east-1.amazonaws.com/sit/v1/notification/update",
                "cognito-domain": "https://cms-sit.auth.ap-southeast-1.amazoncognito.com/oauth2/token",
                "cognito-hago-client-id": "2d64vs1o644bl11b8d4kblh65p",
                "cognito-hago-client-secret": "fii0sskanhgnrq3mnaiukvqpqo78quia7ipm4lk7okio6k2upha",
                "aws_hago_applicationId": "hk.org.ha.testhago",
                "no_message_en": "You have no message.",
                "no_message_zh_hk": "您沒有訊息。",
                "no_service_en": "Service is temporarily unavailable, please try again later.",
                "no_service_zh_hk": "暫時未能提供服務，請稍後再試。"
            }
         }

         let HAGo_props = {
            "hago_auth_info": hago_auth_info,
            "language":language,
            "textsize":'normal',     //HAGo_getFontSize(),
            "app_id": app_id,
            "from_app_id": 'hago'
         }

         // Pass parameter to mini app
         HAGo_init(HAGo_props)
         I18n.locale = language;

        this.props.navigation.navigate(navigatePage)
    }


  render() {
    return (
        <View style={[styles.mainpage,{width:this.state.equipWidth}]} onLayout={this.onLayout} >
                {this.state.loading?(<Loading size='large' color="#00f" />):null}
                <View style={[styles.viewRow]}>
                    
                </View>
                <View style={[styles.viewRow]}>
                    <TouchableOpacity onPress={async() => await HAGo_patient_library()}>
                        <Text style={[styles.textItem]} >HA Go logon</Text>
                    </TouchableOpacity>
                </View>

                <View style={[styles.viewRow]}>
                    <TouchableOpacity onPress={()=>{this.pressNext()}} >
                        <Text style={[styles.textItem]} >Go to apps landing page</Text>
                    </TouchableOpacity>
                    <Badge status="primary" value={this.state.unreadCount == null ? 0 : this.state.unreadCount}/>
                </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
    mainpage: {
        flex: 1,
    },
    viewRow:{
        paddingTop:24,
        paddingLeft:20,
    },
    textItem:{
        fontSize: 20,
        justifyContent: 'center',
        textAlign: 'center',
    },
});

