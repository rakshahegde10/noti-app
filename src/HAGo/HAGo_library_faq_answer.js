import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  Text,
  SectionList,
  StatusBar,
  Platform,
  ScrollView,
  Image,
  Dimensions,
  StyleSheet,
  Linking
} from 'react-native';
import I18n from '../language';

const isIOS = Platform.OS === 'ios';
const {width, height} = Dimensions.get('window');

import { HAGo_getLanguage, HAGo_getFontSize } from './HAGo_authentication';

export default class HAGo_library_faq_answer extends React.Component {

    constructor(props) {
        super(props);

        this.props.navigation.setOptions({ title: I18n.t("faq.title"),headerRight:(props) => (<View></View>) })

        this.state = {
            question:'',
            answer:'',
            type:'',
            //20191115/M/Paul WONG/Implement hyperlink in FAQ Answer
            url:''
        }

        if (this.props.navigation.state.params != undefined) {
            if (this.props.navigation.state.params.question != undefined) {
                this.state.question = this.props.navigation.state.params.question
            }

            if (this.props.navigation.state.params.answer != undefined) {
                this.state.answer = this.props.navigation.state.params.answer
            }

            if (this.props.navigation.state.params.type != undefined) {
                this.state.type = this.props.navigation.state.params.type
            }

            //20191115/M/Paul WONG/Implement hyperlink in FAQ Answer
            if (this.props.navigation.state.params.url != undefined) {
              this.state.url = this.props.navigation.state.params.url
          }
        }
    }

    normalizeFontSize = (fontsize) => {
        let fontScale;
        if (HAGo_getFontSize() == 'small'){
            fontScale = 0.8;
        }
        if (HAGo_getFontSize() == 'normal'){
            fontScale = 1;
        }
        if (HAGo_getFontSize() == 'large'){
            fontScale = 1.2;
        }
        
        return Math.round(fontsize*fontScale);
    }

    setAndroidLineHeight = (fontSize) => {
        //const multiplier = (fontSize > 20) ? 1.5 : 1;
        const multiplier = 1.4;
        return parseInt( (fontSize * multiplier), 10);
    }

    render(){
        let answer_statement_1 = ''
        let answer_statement_2 = ''
        let answer_statement_3 = ''

        if (this.state.answer.includes("|")){
            let tempArr = this.state.answer.split("|")
            answer_statement_1 = tempArr[0]
            answer_statement_2 = tempArr[1]
            answer_statement_3 = tempArr[2]
        }

        return (
              <ScrollView style={{flex:1,backgroundColor:"rgb(255,255,255)"}} >
                  <View style={{paddingTop:24,paddingLeft:20,paddingRight:20}}>
                        <Text style={[{fontSize:this.normalizeFontSize(16),fontWeight:'bold'},Platform.OS === 'android' && I18n.locale!='en' && Platform.Version >= 28?{lineHeight:this.setAndroidLineHeight(this.normalizeFontSize(16))}:'']} >
                            {this.state.question}
                        </Text>
                        
                        {this.state.type != "special" && this.state.type != "url" ? (
                          <View>
                            <Text style={[{fontSize:this.normalizeFontSize(16),paddingTop:20},Platform.OS === 'android' && I18n.locale!='en' && Platform.Version >= 28?{lineHeight:this.setAndroidLineHeight(this.normalizeFontSize(16))}:'']} >
                                {this.state.answer}
                            </Text>
                          </View>
                        ):this.state.type == "url"? (
                          <View style = {{paddingTop:20}}>
                            <Text style={[{fontSize:this.normalizeFontSize(16)},Platform.OS === 'android' && I18n.locale!='en' && Platform.Version >= 28?{lineHeight:this.setAndroidLineHeight(this.normalizeFontSize(16))}:'']} >
                              <Text>{answer_statement_1}</Text>
                              <Text style={{textDecorationLine:'underline',color:'blue'}} onPress={()=>{
                                  Linking.openURL(this.state.url)
                              }}>
                                  {answer_statement_2}
                              </Text>
                              <Text>{answer_statement_3}</Text>
                            </Text>
                          </View>
                        ):null}
                  </View>
              </ScrollView>
        );
    }

}

const styles = StyleSheet.create({
hagoIcon: {
    width: 140,
    height: 100,
    //marginRight: 10,
},
container_hagoIcon: {
    marginTop: 10,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
},
});
 
