﻿/*	************************
	v 1.0.9 @ 28 May 2020
	************************
*/
import React, {Component} from 'react';
import { AsyncStorage, Alert, Platform, Linking } from 'react-native'
import { TouchableOpacity, Image, NativeModules } from "react-native";
import { withNavigation } from '@react-navigation/compat';
import {PermissionsAndroid} from 'react-native';
import PermissionUtil from './common/PermissionUtil.js';
import Permissions from 'react-native-permissions'
//import SInfo from 'react-native-sensitive-info';

import DeviceInfo from 'react-native-device-info';
import Loading from './common/PatientLoading';
import {getLogonData} from './data/getLogonData'

var API_PATH = "https://hago-corp-gateway-sit.appdev.ha.org.hk:16845/hago/gw/2/"

var API_NAME_TOKEN = "api/app/token"
var API_NAME_CONFIG = "api/app/config"
var API_NAME_LOGIN = "api/user/login"
var API_NAME_REFRESHTOKEN = "api/app/refreshtoken"


var API_NAME_Route_Prefix = "api/route?client_API_resource_path="

var API_CHECK_USER_TOKEN = "api/app/checktoken"

var API_NAME_OtpSms="api/user/otp/sms"
var API_NAME_DEVICE_REGISTER="api/user/device/register"
var API_NAME_UndercareRequest = "api/user/carer/profile/undercare"

var GW_34 = "api/user/getFeedBackQuestions"
var GW_33 = "api/user/submitFeedBack"

var already_timeout = false

var CryptoJS = require("crypto-js");
var aes_key_1 = (Math.floor(Math.random() * 1E20) + "") + (Math.floor(Math.random() * 1E20) + "")
if (aes_key_1.length > 32)
    aes_key_1 = aes_key_1.substring(0, 32)
var ivStr = aes_key_1.substring(0, 16)
var jsencrypt = require("./common/jsencrypt.js");

var info = {
  "client_id": "cp-hago-mobile-client",  
  "client_secret": "testing123",
  "device_info": {
    "device_uuid":DeviceInfo.getUniqueId(),
    "device_id":"",
    "device_name": DeviceInfo.getModel(),
    "device_type": Platform.OS === "ios"?"IOS":"ANDROID", // "IOS" / "ANDROID"
    "device_language":"en",
    "bundle_id": DeviceInfo.getBundleId(),
    "app_version": "9.9.8"
  },
  "lang":"en",
  "request_credential": {
      "app_id":"hago",
      "hago_client_id": "cp-hago-mobile-client"
  }
}

errorHandle = {}

var HAGo_settings = {}
var hago_auth_info = {
    "user_id" : 'init_userid',
}
var undercare_info = {
    "is_undercare_mode" : '',
    "undercare_user_id" : '',
    "undercare_profile_pic" : '',
    "undercare_chinese_name" : '',
    "undercare_nickname" : '',
    "undercare_english_name" : '',
    "undercare_color" : '#008A20',
	"undercare_user_role": 'CARER',
}

export async function HAGo_init(props) {

    HAGo_settings = props
	
	if (HAGo_settings.hago_auth_info) {
		info.device_info = HAGo_settings.hago_auth_info.device_info
        info.request_credential.app_id = HAGo_settings.app_id
        
        info.client_id = HAGo_settings.hago_auth_info.client_id
        info.client_secret = HAGo_settings.hago_auth_info.client_secret
        info.request_credential.hago_client_id = HAGo_settings.hago_auth_info.client_id
        
        if (HAGo_settings.language == "zh_hk") {
            info.device_info.device_language = "zh-hk"
        } else if (HAGo_settings.language == "zh_cn") {
            info.device_info.device_language = "zh-cn"
        } else {
            info.device_info.device_language = HAGo_settings.language
        }
        
        info.lang = info.device_info.device_language

        API_PATH = HAGo_settings.hago_auth_info.api_path
    }
	
	if (HAGo_getPushContent() != "") {
    }

}

export function HAGo_getLanguage() {
    return HAGo_settings.language
}

export function HAGo_getFontSize() {
    return HAGo_settings.textsize
}

export function HAGo_getUserID() {
    return HAGo_settings.hago_auth_info.user_id
}

export function HAGo_getSetting(){
    return HAGo_settings
}

export function HAGo_getPushContent() {
    return HAGo_settings.push_content
}

export function HAGo_getLoginStatus() {
    return HAGo_settings.hago_auth_info.user_is_login //Login or null
}

export function HAGo_fingerprintStatus() {
    return HAGo_settings.hago_auth_info.device_info.fingerprint_enabled
}

export function HAGo_getUserStatus() {
    return HAGo_settings.hago_auth_info.user_status
}

export function HAGo_getNavigateMessage() {
    return HAGo_settings.navigate_message
}

export function HAGo_getFromAppID() {
    return HAGo_settings.from_app_id
}

export function HAGo_isUndercareMode() {
    if (HAGo_settings.hago_auth_info.undercare_info.is_undercare_mode != null) {
        if (HAGo_settings.hago_auth_info.undercare_info.is_undercare_mode == 'true') {
            return true
        } else {
            return false
        }
    } else {
        return false
    }
}

export function HAGo_getUndercareInfo() {
    return HAGo_settings.hago_auth_info.undercare_info
}

export function HAGo_getUndercareColor() {
    return HAGo_settings.hago_auth_info.undercare_info.undercare_color
}

export function HAGo_getUndercare_UserID() {
    return HAGo_settings.hago_auth_info.undercare_info.undercare_user_id
}

export function HAGo_getUndercare_UserRole() {
    return HAGo_settings.hago_auth_info.undercare_info.undercare_user_role
}

export function HAGo_getInfo() {
    return info
}

export function HAGo_getUserAccessToken() {
    return HAGo_settings.hago_auth_info.user_access_token
}

export function HAGo_getDeviceInfo() {
    return HAGo_settings.hago_auth_info.device_info
}

export function HAGo_getRSAPublicKey() {
    return HAGo_settings.hago_auth_info.rsa_public_key
}

export function HAGo_getHAAppConfig() {
    return HAGo_settings.hago_auth_info.app_config
}

export async function HAGo_getServerTime() {
    let timestampError = await AsyncStorage.getItem("timestampError")
    let serverTime = parseInt(new Date().getTime()/1000)-Number(timestampError)

    return serverTime
}

export function HAGo_getDeviceUUID() {
    return HAGo_settings.hago_auth_info.device_info.device_uuid
}

export function HAGo_getDeviceID() {
    return HAGo_settings.hago_auth_info.device_info.device_id
}

export async function HAGo_authentication(client_API_resource_path, client_API_post_params) {
    await checkClientAccessToken()

    let data = JSON.parse(JSON.stringify(info))

    data.client_API_post_params = client_API_post_params

    let API_NAME_GetList = API_NAME_Route_Prefix + client_API_resource_path

    let sampleData_response = await getSampleData(data, API_NAME_GetList)

    return sampleData_response
}

export async function HAGo_routing_with_app_id(client_API_resource_path, client_API_post_params, app_id) {
    await checkClientAccessToken()

    let data = JSON.parse(JSON.stringify(info))
    data.client_API_post_params = client_API_post_params
    data.request_credential.app_id = app_id

    let API_NAME_GetList = API_NAME_Route_Prefix + client_API_resource_path

    let sampleData_response = await getSampleData(data, API_NAME_GetList)

    return sampleData_response
}

export async function HAGo_checkUserToken() {
    await checkClientAccessToken()

    var validToken = await HAGo_checkToken()

    if (validToken) {
        let data = JSON.parse(JSON.stringify(info))

        let response = await getCheckUserTokenData(data)

        if (response != undefined) {
            if (response.status == "OK") {
                let result = {
                    valid: true,
                    user_id: HAGo_getUserID()
                }
        
                return result
            }
        }
    }

    let result = {
        valid: false
    }

    return result
}

async function getCheckUserTokenData(data) {

    let timestampError = await AsyncStorage.getItem("timestampError")
    let serverTime =  parseInt(new Date().getTime()/1000)-Number(timestampError)

    let inputFormat = {
        user_access_token: await AsyncStorage.getItem('user_access_token'),
        user_id: HAGo_getUserID(),
        device_info: data.device_info,
        client_id: data.client_id,
        timestamp: serverTime,
        request_credential:data.request_credential
    }

    let url = API_PATH + API_CHECK_USER_TOKEN
    var str = JSON.stringify(inputFormat);

    let parameters = await parametersFromData(str);
    
    return await SubmitRequest(url, parameters, "POST", true);
}

async function getSampleData(data, API_NAME_GetList) {

    var validToken = true

    if (HAGo_getLoginStatus() == "Login") {
        validToken = await HAGo_checkToken();
    }
    
    if (validToken) {

        var stored_user_access_token = null
		
		if (HAGo_getUserStatus() == "CONFIRMED" || HAGo_getUserStatus() == "CONFIRMED_SCODE") {
            stored_user_access_token = await AsyncStorage.getItem('user_access_token')
        }

        let user_access_token = stored_user_access_token
       
        let timestampError = await AsyncStorage.getItem("timestampError")
        let serverTime =  parseInt(new Date().getTime()/1000)-Number(timestampError)

        let is_undercare_mode = HAGo_isUndercareMode()

        var inputFormat = {}
        if (is_undercare_mode) {
            inputFormat = {
                user_access_token:user_access_token,
                user_id:HAGo_getUserID(),
                device_info:data.device_info,
                request_credential:data.request_credential,
                client_API_post_params:data.client_API_post_params,
                timestamp: serverTime,
                undercare_user_id: await HAGo_getUndercareInfo().undercare_user_id
            }
        } else {
            inputFormat = {
                user_access_token:user_access_token,
                user_id:HAGo_getUserID(),
                device_info:data.device_info,
                request_credential:data.request_credential,
                client_API_post_params:data.client_API_post_params,
                timestamp: serverTime
            }
            
        }

        let url = API_PATH + API_NAME_GetList
        var str = JSON.stringify(inputFormat);

        let parameters = await parametersFromData(str);
        
        return await SubmitRequest(url, parameters, "POST",true);
    }
}

async function parametersFromData(string) {
  
  let aesEncryptParamStr = aesEncrypt(string)
  let content = aesEncryptParamStr
  let rsa_aes_key = await RSAJSEncrypt()
  
  let lang = info.lang;

  let parameters = new FormData();
  parameters.append('content', content);
  parameters.append('en_key', rsa_aes_key);
  parameters.append("lang", lang)

  return parameters
}

function aesEncrypt(paramStr) {

  var key = CryptoJS.enc.Utf8.parse(aes_key_1);
  var iv = CryptoJS.enc.Utf8.parse(ivStr);
  const cipher = CryptoJS.AES.encrypt(paramStr, key, {
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7,
      iv: iv,
  });

  const base64Cipher = CryptoJS.enc.Base64.stringify(cipher.ciphertext);
  return base64Cipher
}

async function RSAJSEncrypt() {
  let rsa_public_key = await AsyncStorage.getItem('rsa_public_key');
  var crypt = new JSEncrypt();
  crypt.setPublicKey(rsa_public_key);

  var rsa_aes_key = crypt.encrypt(aes_key_1);

  return rsa_aes_key
}

function aesDecrypt(decryptStr) {
  try {

      var iv = CryptoJS.enc.Utf8.parse(ivStr);

      var key = CryptoJS.enc.Utf8.parse(aes_key_1);

      const decipher = CryptoJS.AES.decrypt(decryptStr, key, {
          mode: CryptoJS.mode.CBC,
          padding: CryptoJS.pad.Pkcs7,
          iv: iv,
      });

      const resultDecipher = CryptoJS.enc.Utf8.stringify(decipher);
      return resultDecipher
  } catch (e) {
      return "{}";
  }

}

async function SubmitRequest(url, inputBody, method, isEncode) {

  var headers = {
      'Accept': 'application/json',
  }
  
  let client_access_token = await AsyncStorage.getItem('client_access_token')
  let user_access_token = await AsyncStorage.getItem('user_access_token')


  if (url != API_PATH + API_NAME_TOKEN) {
          headers.Authorization = "Bearer " + client_access_token
  }




  try {

      if (method == 'GET') {
          var url = url + "?" + inputBody
          var response = await fetch(url)
      } else {
          var response = await fetch(url, {
              method: method,
              headers: headers,
              body: inputBody
          })
      }

      var result = await response.json();

      switch (result.status_code) {
            //client_access_token expires
            case "1001":

                let tokenResult = await token()

                let client_access_token = tokenResult.client_access_token;
                AsyncStorage.setItem('client_access_token',client_access_token)

                let client_access_token_expr = tokenResult.access_token_expr + ""
                AsyncStorage.setItem('client_access_token_expr', client_access_token_expr)

                if (HAGo_getLanguage() == "en") {
                    forceBackToHAGo("Please login again")
                } else {
                    forceBackToHAGo("請再登入")
                }
            case "1002":
                let err_1002 = new Error
                err_1002.name = result.status_code;
                err_1002.message = result.status_message
                if (err_1002.message ==""){
                    err_1002.message = "login session expired"
                }

                if (HAGo_getLanguage() == "en") {
                    forceBackToHAGo("Login session timeout, please login again")
                } else {
                    forceBackToHAGo("登入時限已過，請再登入")
                }
                break;
            case "2008":
            case "2055":
            case "2056":
			case "8001": //Telecare service is not available
                    return result;
                    break;
            default:
                if (result.status_code != 0) {
                    var errormsg = ""
                    if (errorHandle[result.status_code]) {
                        errormsg = errorHandle[result.status_code]
                    } else {
                        errormsg = "[" + result.status_code + "]" + result.status_message //== "" ? "[" + result.status_code + "] no message or message is null" : result.status_message
                        if (result.status_code == "2040") {
                            NavigationService.navigate('forgotUsername')
                        }
                    }
                    let err = new Error
                    err.name = result.status_code;
                    err.message = errormsg


                    throw err;
                } else {
                    var resultStr = result.result

                    if(result.result==null){
                        return {};
                    }
                    var decryptedResult = JSON.stringify(resultStr)
                    if (isEncode)
                        decryptedResult = aesDecrypt(resultStr)

                    var decryptedResultObj = JSON.parse(decryptedResult)

                    if (typeof(decryptedResultObj) == "object" && typeof(decryptedResultObj.tokens) != "undefined") {
                        var tokensObj = decryptedResultObj.tokens[0]
                        decryptedResultObj.access_token = tokensObj.access_token;
                        decryptedResultObj.refresh_token = tokensObj.refresh_token;
                        decryptedResult = JSON.stringify(decryptedResultObj)
                        if(tokensObj.access_token && tokensObj.refresh_token){
                            AsyncStorage.setItem('user_access_token', tokensObj.access_token)
                            AsyncStorage.setItem('user_refresh_token', tokensObj.refresh_token)
                        } else{
                        }
                    }

                    return JSON.parse(decryptedResult);
                }


                break
      }

  } catch (e) {
      Alert.alert(e.message)
      throw e;
  }
}

export function navigateToOtherApp(from_app_id, to_app_id, navigate_message) {

	if (to_app_id == 'telecare') {
        //20200915/M/Paul WONG/v1.2.8 - Implement the permission checking for telecare
        telecarePermissionChecking(from_app_id, to_app_id, navigate_message)
    } else {
		
		let message = 'from_app_id: ' + from_app_id + '\nto_app_id: ' + to_app_id + '\nnavigate_message: ' + navigate_message

		Alert.alert(
			'',
			message,
			[{
				text: HAGo_getLanguage() == 'en'?'OK':'確定',
				onPress: () => {}
			}],
			{ cancelable: false }
		)
	}
}

export function HAGo_setNativeStorage(key, value) {
    if (Platform.OS === 'ios') {
        var NativeManager = NativeModules.NativeManager;
        NativeManager.setNativeStorage(key, value);
    } else if (Platform.OS === 'android') {
        NativeModules.HAGoBridge.setNativeStorage(key, value);
    }
}

export function HAGo_getNativeStorage(key) {
    var result = ""

    if (Platform.OS === 'ios') {
        var NativeManager = NativeModules.NativeManager;
        result = NativeManager.getNativeStorage(key);
    } else if (Platform.OS === 'android') {
        result = NativeModules.HAGoBridge.getNativeStorage(key);
    }

    return result
}

export function HAGo_removeNativeStorage(key) {
    if (Platform.OS === 'ios') {
        var NativeManager = NativeModules.NativeManager;
        NativeManager.removeNativeStorage(key);
    } else if (Platform.OS === 'android') {
        NativeModules.HAGoBridge.removeNativeStorage(key);
    }
}

export async function HAGo_external_parameters(mini_app_client_id) {
	await checkClientAccessToken()
    await HAGo_checkToken()

    let timestampError = await AsyncStorage.getItem("timestampError")
    let serverTime =  parseInt(new Date().getTime()/1000)-Number(timestampError)

    let data = JSON.parse(JSON.stringify(info))

    data.request_credential.hago_client_id = mini_app_client_id

    /*let inputFormat = {
        user_access_token: await AsyncStorage.getItem('user_access_token'),
        user_id: HAGo_getUserID(),
        device_info: data.device_info,
        client_id: data.client_id,
        timestamp: serverTime,
        request_credential: data.request_credential
    }*/

    var inputFormat = {}
    if (!HAGo_isUndercareMode()) {
        inputFormat = {
            user_access_token: await AsyncStorage.getItem('user_access_token'),
            user_id: HAGo_getUserID(),
            device_info: data.device_info,
            client_id: data.client_id,
            timestamp: serverTime,
            request_credential: data.request_credential
        }
    } else {
        inputFormat = {
            user_access_token: await AsyncStorage.getItem('user_access_token'),
            user_id: HAGo_getUserID(),
            undercare_user_id: HAGo_getUndercare_UserID(),
            device_info: data.device_info,
            client_id: data.client_id,
            timestamp: serverTime,
            request_credential: data.request_credential
        }
    }

    let url = API_PATH + API_CHECK_USER_TOKEN
    var str = JSON.stringify(inputFormat);

    let parameters = await HAGo_get_external_parameters(str);
    
    return parameters
}

async function HAGo_get_external_parameters(str) {
    let data = JSON.parse(JSON.stringify(info))

    /*let HAGo_external_parameters = {
        hago_parameters: await parametersFromData(str),
        user_id: HAGo_getUserID()
    }*/

    let HAGo_external_parameters = {}

    if (!HAGo_isUndercareMode()) {
        HAGo_external_parameters = {
            hago_parameters: await parametersFromData(str),
            user_id: HAGo_getUserID()
        }
    } else {
        HAGo_external_parameters = {
            hago_parameters: await parametersFromData(str),
            user_id: HAGo_getUserID(),
            undercare_user_id: HAGo_getUndercare_UserID(),
            hago_user_role: HAGo_getUndercare_UserRole()
        }
    }
  
    return HAGo_external_parameters
  }

class HAGo_BackButton extends React.Component{
	constructor(props) {
        super(props)

        this.state = {
            clear_sensitive_info_key: ''
        };

        if (this.props.clear_sensitive_info_key != undefined) {
            this.state.clear_sensitive_info_key = this.props.clear_sensitive_info_key
        }
    }
	
	_backButtonDidClick= async() => {
		if (this.state.clear_sensitive_info_key != undefined && this.state.clear_sensitive_info_key != '') {
            let key_array = this.state.clear_sensitive_info_key.split('|')

            for (var i = 0; i <= key_array.length - 1; i++) {
                //await SInfo.deleteItem(key_array[i], {})
            }
        }

        /*if (Platform.OS === 'ios') {
            var NativeManager = NativeModules.NativeManager;
            NativeManager.appointmentBackButtonDidClick();
        } else if (Platform.OS === 'android') {
            NativeModules.HAGoBridge.closeCurrentApp();
        }*/

        this.props.navigation.goBack()
	}

	render(){
		return (
			<TouchableOpacity onPress={ ()=>{this._backButtonDidClick()}} style={{paddingLeft:18,width:56,height:56,justifyContent:"center"}}>
				<Image source={require('./images/HAGo_back_button.png')} style={{width:54,height:26}}/>
			</TouchableOpacity>
		)
	}
}

export default withNavigation(HAGo_BackButton);

export function HAGo_backAction() {
	/*if (Platform.OS === 'ios') {
		var NativeManager = NativeModules.NativeManager;
		NativeManager.appointmentBackButtonDidClick();
	} else if (Platform.OS === 'android') {
		NativeModules.HAGoBridge.closeCurrentApp();
	}*/
	
	Alert.alert(
        '',
        HAGo_getLanguage() == 'en'?'Please press the back button on the top left corner':'請按左上角的返回按鈕',
        [{
            text: HAGo_getLanguage() == 'en'?'OK':'確定',
            onPress: () => {}
        }],
        { cancelable: false }
    )
}

async function forceBackToHAGo(msg) {
    if (!already_timeout) {
        already_timeout = true

        var button_msg = ""

        if (HAGo_getLanguage() == "en") {
            button_msg = "OK"
        } else {
            button_msg = "確定"
        }

        Alert.alert(
            msg,
            '',
            [
              {text: button_msg, onPress: () => {
                if (Platform.OS === 'ios') {
                    var NativeManager = NativeModules.NativeManager;
                    NativeManager.clientAppTimeout()
                } else if (Platform.OS === 'android') {
                    NativeModules.HAGoBridge.clientAppTimeout();
                }
              }}
            ],
            {cancelable: false},
        );
    }
}
export async function HAGo_checkToken() {
    let current_expires_time = await AsyncStorage.getItem('expires_time')

    // debugger
    let goToLogout=false;
    if (current_expires_time) {
        let deviceTime= await AsyncStorage.getItem("device_time")-0//device time
        let loginTime=await AsyncStorage.getItem("login_time")-0//login sever time

        let nowTime=(loginTime-0)+((new Date().getTime()/1000-deviceTime)-0);//now sever time
  
        let lastActionTime= await AsyncStorage.getItem("lastActionTime")-0//last Action time
  
  
        let expires_time= await AsyncStorage.getItem('expires_time')-0 //refresh token expiry time
        let access_expires_time= await AsyncStorage.getItem('access_token_expr')-0//access token expiry time
  
        let logonTime=nowTime-loginTime//logon time
        let idleTime =(!lastActionTime?0:nowTime-lastActionTime)//idleTime
  
        if(!lastActionTime){
            await AsyncStorage.setItem("lastActionTime",nowTime+"")
        }else{
            if(idleTime>(access_expires_time-loginTime)){
                //logout
                if(logonTime+idleTime>=(access_expires_time-loginTime)&&logonTime+idleTime<=(expires_time-loginTime)){
                    let check = await checkExpiresTime(current_expires_time);
                }else{
                    if (!HAGo_fingerprintStatus()) {
                        await AsyncStorage.removeItem("username")
                        //await SInfo.deleteItem("password",{})
                    }
  
                    await AsyncStorage.removeItem("user_status")
                    await AsyncStorage.removeItem('user_is_login')
                    await AsyncStorage.removeItem("user_access_token")
                    await AsyncStorage.removeItem("user_id")
                        
                    await AsyncStorage.removeItem("lastActionTime")
                    await AsyncStorage.removeItem("login_time")
                    await AsyncStorage.removeItem("expires_time")
                    await AsyncStorage.removeItem("access_token_expr")
                    await AsyncStorage.removeItem("user_refresh_token")
                    await AsyncStorage.removeItem("device_time")

                    if (HAGo_getLanguage() == "en") {
                        forceBackToHAGo("Login session timeout, please login again")
                    } else {
                        forceBackToHAGo("登入時限已過，請再登入")
                    }

                    return false

                }
            }else{//actionTime-nowTime<overdueTime
                if(logonTime+idleTime<(access_expires_time-loginTime)){
                    await AsyncStorage.setItem("lastActionTime",nowTime+"")
                }else if(logonTime+idleTime>=(access_expires_time-loginTime)&&logonTime+idleTime<=(expires_time-loginTime)){
                    let check = await checkExpiresTime(current_expires_time);
                }else{
                }
            }
        }
    } else {
        if (HAGo_getLanguage() == "en") {
            forceBackToHAGo("Please login again")
        } else {
            forceBackToHAGo("請再登入")
        }
        return false
    }

    return true
}

export async function checkExpiresTime(current_expires_time) {
    return new Promise(async(resolve, reject) => {
        let isExpires = false
        if (current_expires_time && new Date().getTime()/1000 < (current_expires_time - 0)) {

            
            let access_tokenObj = await refreshtoken()

            var tokens = access_tokenObj;

            let user_access_token = tokens.access_token;
            let user_refresh_token = tokens.refresh_token;
            if (user_access_token && user_refresh_token) {
                    await AsyncStorage.setItem('user_access_token', user_access_token)
                    await AsyncStorage.setItem('user_refresh_token', user_refresh_token)
			}else {
			}

            let expires_time = tokens.tokens[0].refresh_token_expr;
            let access_expires_time = tokens.tokens[0].access_token_expr;

            await AsyncStorage.setItem('expires_time', expires_time + "")
            await AsyncStorage.setItem('access_token_expr',access_expires_time+"")

            isExpires = true
            resolve(isExpires);
        } else {
            isExpires = false
            resolve(isExpires)
        }
    })
}

export async function refreshtoken() {
    let data = JSON.parse(JSON.stringify(info));
    let pushToken = await AsyncStorage.getItem('token')
    data.device_info.device_id = (pushToken == null) ? "" : pushToken

    let user_id = HAGo_getUserID()
    let user_refresh_token = await AsyncStorage.getItem('user_refresh_token')

    let timestampError = await AsyncStorage.getItem("timestampError")
    let serverTime =  parseInt(new Date().getTime()/1000)-Number(timestampError)

    let inputFormat = {
        user_refresh_token: user_refresh_token,
        user_id: user_id,
        device_info: info.device_info,
        request_credential: info.request_credential,
        timestamp: serverTime
    }
    let url = API_PATH + API_NAME_REFRESHTOKEN;
    var str = JSON.stringify(inputFormat);
    let parameters = await parametersFromData(str);

    return await call(url, parameters, "POST", true);
}

export async function call(url, inputBody, method, isEncode) {
    var headers = {
        'Accept': 'application/json',
    }
    let client_access_token = await AsyncStorage.getItem('client_access_token')
    headers.Authorization = "Bearer " + client_access_token; //+"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiaGFnb19hcHAiXSwiaGFnb191dWlkIjoiZGV2aWNlMDAiLCJzY29wZSI6WyJyZWFkIiwiIHdyaXRlIl0sImV4cCI6MTU0NDEzNzYxMSwiYXV0aG9yaXRpZXMiOlsiUk9MRV9IQUdPX01PQklMRV9DTElFTlQiLCJST0xFX0VYVEVSTkFMX0NMSUVOVCJdLCJqdGkiOiIzMjUxZDc3OC1mNDRlLTRlYjYtOTgwZC1mZDE4MDAzMjgyOWYiLCJjbGllbnRfaWQiOiJjcC1oYWdvLW1vYmlsZS1jbGllbnQifQ.CEupD_WVcj9DWy8a87J8o7ISlXf0Ug92AtY84Ma61QwAjpcUeydSAeqg9y2t7UFVc8VNn5daUNpazweWglYEHDVHUMigHa0Hk3B0ols6umuszzpYNAvDSLM8iVtzxu45gsoJPlcaG2-LsEbjhaklHccrQoUSQ9kmq3DzvjvWW6-C9zjH-KM0Q9RQt1IfPqiBvj34W2AVqLOl_NEv0FhBIePDeyitgiovm8M02WAxv9vnvI9WP_k6vxU-alCkAn35VzDFR-fbtWPKfy9aXPiYf_jpNHir7x4AGbWg80nISrdlkjvSCe9T9SHv1uBiJhy9vx7yA6c_zbw2AhmUiXWbMQ";

        headers.Authorization = "Bearer " + client_access_token

    try {

        if (method == 'GET') {
            var url = url + "?" + inputBody
            var response = await fetch(url)
        } else {
            var response = await fetch(url, {
                method: method,
                headers: headers,
                body: inputBody
            })

        }

        var result = await response.json();
        switch (result.status_code) {
            default:
                var resultStr = result.result
                var decryptedResult = JSON.stringify(resultStr)
                if (isEncode)
                    decryptedResult = aesDecrypt(resultStr)

                var decryptedResultObj = JSON.parse(decryptedResult)

                if (typeof(decryptedResultObj) == "object" && typeof(decryptedResultObj.tokens) != "undefined") {
                    var tokensObj = decryptedResultObj.tokens
                    decryptedResultObj.access_token = tokensObj[0].access_token;
                    decryptedResultObj.refresh_token = tokensObj[0].refresh_token;
                    decryptedResult = JSON.stringify(decryptedResultObj)

                    if(decryptedResultObj.access_token && decryptedResultObj.refresh_token){
                      AsyncStorage.setItem('user_access_token', decryptedResultObj.access_token)
                      AsyncStorage.setItem('user_refresh_token', decryptedResultObj.refresh_token)
					}else{
					}
                }

                return JSON.parse(decryptedResult);
                break
        }

    } catch (e) {
        return {
            status: "fail",
            message: "MOBILE_ERROR_UNKNOWN",
            description: e
        }
    }
}

export async function checkClientAccessToken() {
    let client_access_token_expr = await AsyncStorage.getItem('client_access_token_expr')


    let current_time = new Date().getTime()/1000

    if (current_time >= client_access_token_expr) {
        let tokenResult = await token()

        let client_access_token = tokenResult.client_access_token;
        AsyncStorage.setItem('client_access_token',client_access_token)

        let new_client_access_token_expr = tokenResult.access_token_expr + ""
        AsyncStorage.setItem('client_access_token_expr', new_client_access_token_expr)
    } else {
    }
}

export async function token() {
    let data = JSON.parse(JSON.stringify(info));
    let pushToken = await AsyncStorage.getItem('token')
    data.device_info.device_id = (pushToken == null) ? "" : pushToken

    let timestampError = await AsyncStorage.getItem("timestampError")
    let serverTime =  parseInt(new Date().getTime()/1000)-Number(timestampError)

    let inputFormat = {
        client_id: data.client_id,
        client_secret: data.client_secret,
        device_info: data.device_info,
        request_credential: data.request_credential,
        timestamp: serverTime
    }

    let url = API_PATH + API_NAME_TOKEN
    var str = JSON.stringify(inputFormat);

    let parameters = new FormData();
    parameters.append('content', str);
    parameters.append('lang', info.lang);
    return await SubmitRequest(url, parameters, "POST");
}

// ******************************************** //
// ************* Patient Library ************** //
// ******************************************** //
patientLibraryLoadingManager = Loading.ShareInstance()

export async function HAGo_patient_library(props) {
    await clearPatientLibrarySession()

    let param = await getLogonData()

    Alert.alert("Choose Patient", '', [{
        text: "Close",
        onPress: () => {
        }
      },{
        text: "Normal patient",
        onPress: () => {
            getPatientInfo(param.username,param.password)
        }
      },{
        text: "Patient with carer",
        onPress: () => {
            getPatientInfoWithCarer(param.username,param.password,param.carerid)
        }
      }],
      {cancelable: false}
    )
}

// ******************************************** //
// ************* Common Functions ************* //
// ******************************************** //
function getMessagePrompt(){
    patientLibraryLoadingManager.setLoading(false)

    hago_auth_info.undercare_info = undercare_info
    HAGo_settings.hago_auth_info = hago_auth_info

    Alert.alert("Login success", '', [{
        text: "OK",
        onPress: () => {
            getHAGoLanguage()
        }
        }],
        {cancelable: false}
    )
}
async function getPatientInfo(username, password) {
    patientLibraryLoadingManager.setLoading(true)
    
    await clearUndercareProfile()

    let clientAccessToken_response = await getPatientLibraryClientAccessToken()
    AsyncStorage.setItem('client_access_token', clientAccessToken_response.client_access_token)

    let config_response = await getPatientLibraryConfig()
    AsyncStorage.setItem('rsa_public_key',config_response.gw_public_key)

    let data = info

    data.username = username
    data.password = password

    login(data).then(async(json) => {
        
        hago_auth_info.user_id = json.user_id
        
        if (json.status_code == "2008"){
            let mobile_phone_no = json.result.mobile_phone_no
            let apiData = info
            apiData.username = username
            apiData.password = password
    
            OtpSms(apiData).then(json => {

                AsyncStorage.setItem("username",apiData.username )

                let activateData = info
                activateData.register_otp = '111111'

                deviceRegister(activateData).then(async(json) => {

                    await setPatientLibrarySession(json)
                    getMessagePrompt()

                }).catch(err => {
                    patientLibraryLoadingManager.setLoading(false)
                })

            }).catch(err => {
                patientLibraryLoadingManager.setLoading(false)
            })
        } else if (json.status_code == "2055" || json.status_code == "2056") {
            let mobile_phone_no = json.result.mobile_phone_no

            let apiData = info;
            apiData.username = username
            apiData.password = password

            OtpSms(apiData).then(json => {
                AsyncStorage.setItem("username",data.username )

                let activateData = info
                activateData.username = username
                activateData.password = password
                activateData.register_otp = '111111'

                deviceRegister(activateData).then(async(json) => {

                    await setPatientLibrarySession(json)

                    getMessagePrompt()

                }).catch(err => {
                    patientLibraryLoadingManager.setLoading(false)
                })
            }).catch(err => {
                patientLibraryLoadingManager.setLoading(false)
            })
            
            
        } else {
            // Normal case
            await setPatientLibrarySession(json)
            AsyncStorage.setItem("username",data.username)

            getMessagePrompt()
        }
    }).catch(err =>{
        patientLibraryLoadingManager.setLoading(false)
    })

}

async function getPatientInfoWithCarer(username, password, carer_id) {
    patientLibraryLoadingManager.setLoading(true)

    let clientAccessToken_response = await getPatientLibraryClientAccessToken()
    AsyncStorage.setItem('client_access_token', clientAccessToken_response.client_access_token)

    let config_response = await getPatientLibraryConfig()
    AsyncStorage.setItem('rsa_public_key',config_response.gw_public_key)

    let data = info

    data.username = username
    data.password = password

    login(data).then(async(json) => {
        if (json.status_code == "2008"){
            let mobile_phone_no = json.result.mobile_phone_no
            let apiData = info
            apiData.username = username
            apiData.password = password
    
            OtpSms(apiData).then(json => {

                AsyncStorage.setItem("username",apiData.username )

                let activateData = info
                activateData.register_otp = '111111'

                deviceRegister(activateData).then(async(json) => {

                    await setPatientLibrarySession(json)

                    getCarerInfo(carer_id)

                }).catch(err => {
                    patientLibraryLoadingManager.setLoading(false)
                })

            }).catch(err => {
                patientLibraryLoadingManager.setLoading(false)
            })
        } else if (json.status_code == "2055" || json.status_code == "2056") {
            let mobile_phone_no = json.result.mobile_phone_no

            let apiData = info;
            apiData.username = username
            apiData.password = password

            OtpSms(apiData).then(json => {
                AsyncStorage.setItem("username",data.username )

                let activateData = info
                activateData.username = username
                activateData.password = password
                activateData.register_otp = '111111'

                deviceRegister(activateData).then(async(json) => {

                    await setPatientLibrarySession(json)

                    getCarerInfo(carer_id)

                }).catch(err => {
                    patientLibraryLoadingManager.setLoading(false)
                })
            }).catch(err => {
                patientLibraryLoadingManager.setLoading(false)
            })
            
            
        } else {
            // Normal case
            await setPatientLibrarySession(json)
            AsyncStorage.setItem("username",data.username)

            getCarerInfo(carer_id)
        }
    }).catch(err =>{
        patientLibraryLoadingManager.setLoading(false)
    })
    
}

async function getCarerInfo(carer_id) {
    underCareRequest(carer_id).then(async json => {

        AsyncStorage.setItem('in_undercare_mode', 'true')

        if (json.user_id != null) {
            await AsyncStorage.setItem('undercare_user_id', json.user_id)
        }

        if (json.profile_base64_img != null) {
            await AsyncStorage.setItem('undercarer_pic', json.profile_base64_img)
        }

        if (json.chinese_name_image != null) {
            await AsyncStorage.setItem('undercarer_chinese_name', json.chinese_name_image)
        }

        if (json.nickname != null) {
            await AsyncStorage.setItem('undercarer_nickname', json.nickname)
        }

        if (json.english_name != null) {
            await AsyncStorage.setItem('undercarer_english_name', json.english_name)
        }
		
		if (json.relationship != null) {
            await AsyncStorage.setItem('undercare_user_role', json.relationship)
        }
        
        undercare_info.is_undercare_mode = 'true'
        undercare_info.undercare_user_id = json.user_id
        undercare_info.undercarer_pic = json.profile_base64_img
        undercare_info.undercarer_chinese_name = json.chinese_name_image
        undercare_info.undercarer_nickname = json.nickname
        undercare_info.undercarer_english_name = json.english_name
		undercare_info.undercare_user_role = json.relationship

        getMessagePrompt()
    }).catch(err => {
        patientLibraryLoadingManager.setLoading(false)
    })
}

async function clearUndercareProfile() {
    await AsyncStorage.removeItem('in_undercare_mode')
    await AsyncStorage.removeItem('undercare_user_id')
    await AsyncStorage.removeItem('undercarer_pic')
    await AsyncStorage.removeItem('undercarer_chinese_name')
    await AsyncStorage.removeItem('undercarer_nickname')
    await AsyncStorage.removeItem('undercarer_english_name')
	await AsyncStorage.removeItem('undercare_user_role')
    
    undercare_info.is_undercare_mode = 'false'
    undercare_info.undercare_user_id = ''
    undercare_info.undercarer_pic = ''
    undercare_info.undercarer_chinese_name = ''
    undercare_info.undercarer_nickname = ''
    undercare_info.undercarer_english_name = ''
	undercare_info.undercare_user_role = 'CARER'
    hago_auth_info.undercare_info = undercare_info
    HAGo_settings.hago_auth_info = hago_auth_info

}

async function getHAGoLanguage() {
    Alert.alert("Choose Language", '', [{
        text: "中文",
        onPress: () => {
            HAGo_settings.language = 'zh_hk'
            getHAGoFontSize()
        }
      },{
        text: "English",
        onPress: () => {
            HAGo_settings.language = 'en'
            getHAGoFontSize()
        }
      }],
      {cancelable: false}
    )
}

async function getHAGoFontSize() {
    Alert.alert("Choose Font Size", '', [{
        text: "Small",
        onPress: () => {
            HAGo_settings.textsize = 'small'
        }
      },{
        text: "Normal",
        onPress: () => {
            HAGo_settings.textsize = 'normal'
        }
      },{
        text: "Large",
        onPress: () => {
            HAGo_settings.textsize = 'large'
        }
      }],
      {cancelable: false}
    )
}

// ******************************************** //
// ************* Static Functions ************* //
// ******************************************** //

export async function clearPatientLibrarySession() {
    await AsyncStorage.removeItem("user_id")
    await AsyncStorage.removeItem("username")
        
    await AsyncStorage.removeItem('user_is_login')
    await AsyncStorage.removeItem("user_status")
  
    //await SInfo.deleteItem("password",{})
  
    await AsyncStorage.removeItem("user_access_token")
    await AsyncStorage.removeItem("user_refresh_token")
    await AsyncStorage.removeItem("access_token_expr")
    await AsyncStorage.removeItem("expires_time")
  
    await AsyncStorage.removeItem("lastActionTime")
    await AsyncStorage.removeItem("login_time")
    await AsyncStorage.removeItem("device_time")
  
    await AsyncStorage.removeItem("prescribe_app")
  
    await AsyncStorage.removeItem("last_name")
    await AsyncStorage.removeItem("first_name")
    await AsyncStorage.removeItem("mobile_phone_no")
  
    await AsyncStorage.removeItem("doc_no")
    await AsyncStorage.removeItem("email_address")

    hago_auth_info.user_id = ''
    undercare_info.is_undercare_mode = 'false'
    undercare_info.undercare_user_id = ''
    undercare_info.undercarer_pic = ''
    undercare_info.undercarer_chinese_name = ''
    undercare_info.undercarer_nickname = ''
    undercare_info.undercarer_english_name = ''
    hago_auth_info.undercare_info = undercare_info
    HAGo_settings.hago_auth_info = hago_auth_info
}
  
export async function setPatientLibrarySession(json_results) {
    
    let user_access_token = json_results.access_token;
    let user_refresh_token = json_results.refresh_token;
    
    await AsyncStorage.setItem('user_access_token', user_access_token)
    await AsyncStorage.setItem('user_refresh_token', user_refresh_token)
    await AsyncStorage.setItem('user_id', json_results.user_id)
    await AsyncStorage.setItem('user_is_login', 'Login')
  
    let expires_time = json_results.tokens[0].refresh_token_expr;
    let access_expires_time = json_results.tokens[0].access_token_expr;
    let login_time = json_results.tokens[0].iat;
  
    let device_time = new Date().getTime()/1000
  
    await AsyncStorage.setItem('access_token_expr', access_expires_time+"")
    await AsyncStorage.setItem('login_time', login_time+"")
    await AsyncStorage.setItem('device_time', device_time+"")
    await AsyncStorage.setItem('expires_time', expires_time.toString())
    await AsyncStorage.setItem('lastActionTime','')

    await AsyncStorage.setItem('user_status', 'CONFIRMED')
}

// ******************************************** //
// *************** API calls ****************** //
// ******************************************** //

async function getPatientLibraryClientAccessToken() {
    let data = info;
  
    let timestampError = await AsyncStorage.getItem("timestampError")
    let serverTime =  parseInt(new Date().getTime()/1000)-Number(timestampError)

    let inputFormat = {
        client_id: data.client_id,
        client_secret: data.client_secret,
        device_info: data.device_info,
        request_credential: data.request_credential,
        timestamp: serverTime
    }
  
    let url = API_PATH + API_NAME_TOKEN
    var str = JSON.stringify(inputFormat);
  
    let parameters = new FormData();
    parameters.append('content', str);
    parameters.append('lang', info.lang);
    return await SubmitRequest(url, parameters, "POST");
}

async function getPatientLibraryConfig() {
    let data = info;
  
    let inputFormat = {
        config_type: data.client_id,
        device_info: data.device_info,
        request_credential: data.request_credential
    }
  
    let url = API_PATH + API_NAME_CONFIG
    var str = JSON.stringify(inputFormat);
  
    let parameters = new FormData();
    parameters.append('content', str);
    parameters.append('lang', info.lang);
    return await SubmitRequest(url, parameters, "POST");
}

export async function checkPatientLibraryUserAccessToken() {
    let current_expires_time = await AsyncStorage.getItem('expires_time')
    let goToLogout=false;
    if (current_expires_time){
        let deviceTime= await AsyncStorage.getItem("device_time")-0//device time
        let loginTime=await AsyncStorage.getItem("login_time")-0//login sever time
  
        let nowTime=(loginTime-0)+((new Date().getTime()/1000-deviceTime)-0);//now sever time
  
        let lastActionTime= await AsyncStorage.getItem("lastActionTime")-0//last Action time
  
  
        let expires_time= await AsyncStorage.getItem('expires_time')-0 //refresh token expiry time
        let access_expires_time= await AsyncStorage.getItem('access_token_expr')-0//access token expiry time
  
        let logonTime=nowTime-loginTime//logon time
        let idleTime =(!lastActionTime?0:nowTime-lastActionTime)//idleTime
  
        if(!lastActionTime){
            await AsyncStorage.setItem("lastActionTime",nowTime+"")
		}else{
            if(idleTime>(access_expires_time-loginTime)){
                //logout
                if(logonTime+idleTime>=(access_expires_time-loginTime)&&logonTime+idleTime<=(expires_time-loginTime)){
                    let check = await checkExpiresTime(current_expires_time);
                }else{
                    await clearPatientLibrarySession()
  
                    return true
                }
            }else{//actionTime-nowTime<overdueTime
                if(logonTime+idleTime<(access_expires_time-loginTime)){
                    await AsyncStorage.setItem("lastActionTime",nowTime+"")
                }else if(logonTime+idleTime>=(access_expires_time-loginTime)&&logonTime+idleTime<=(expires_time-loginTime)){
                    let check = await checkExpiresTime(current_expires_time);
                }else{
                }
            }
        }
    }
  
    return false
  }



export async function login(data) {
    await checkClientAccessToken();
    await AsyncStorage.removeItem("lastActionTime")
    await AsyncStorage.removeItem("login_time")
    await AsyncStorage.removeItem("expires_time")
    await AsyncStorage.removeItem("access_token_expr")
    await AsyncStorage.removeItem("device_time")

    let timestampError = await AsyncStorage.getItem("timestampError")
    let serverTime =  parseInt(new Date().getTime()/1000)-Number(timestampError)

    let inputFormat = {
        username: data.username,
        password: data.password,
        device_info: data.device_info,
        request_credential: data.request_credential,
        timestamp: serverTime
    }

    let url = API_PATH + API_NAME_LOGIN
    var str = JSON.stringify(inputFormat);
    let parameters = await parametersFromData(str)

    return await SubmitRequest(url, parameters, "POST", true);
}

export async function OtpSms(data) {
    await checkClientAccessToken();

    let timestampError = await AsyncStorage.getItem("timestampError")
    let serverTime =  parseInt(new Date().getTime()/1000)-Number(timestampError)

    let inputFormat = {
        username: data.username,
        password: data.password,
        device_info: data.device_info,
        request_credential: data.request_credential,
        timestamp: serverTime
    }

    let url = API_PATH + API_NAME_OtpSms;
    var str = JSON.stringify(inputFormat);

    let parameters = await parametersFromData(str);

    return await SubmitRequest(url, parameters, "POST", true);
}

export async function deviceRegister() {
    await checkClientAccessToken();

    let data = info;
    let pushToken = await AsyncStorage.getItem('token')
    data.device_info.device_id = (pushToken == null) ? "" : pushToken

    let timestampError = await AsyncStorage.getItem("timestampError")
    let serverTime =  parseInt(new Date().getTime()/1000)-Number(timestampError)

    let inputFormat = {
        username: data.username,
        password: data.password,
        device_info: data.device_info,
        request_credential: data.request_credential,
        activate_otp: data.register_otp,
        timestamp: serverTime
    }
    let url = API_PATH + API_NAME_DEVICE_REGISTER;
    var str = JSON.stringify(inputFormat);
    let parameters = await parametersFromData(str);

    return await SubmitRequest(url, parameters, "POST", true);
}

export async function underCareRequest(undercare_id) {
    await checkClientAccessToken();

    await checkPatientLibraryUserAccessToken();

    let data = info;
    let user_id = await AsyncStorage.getItem('user_id')
    hago_auth_info.user_id = user_id
    let user_access_token = await AsyncStorage.getItem('user_access_token')

    let timestampError = await AsyncStorage.getItem("timestampError")
    let serverTime = parseInt(new Date().getTime() / 1000) - Number(timestampError)

    let inputFormat = {
        user_access_token: user_access_token,
        user_id: user_id,
        undercare_user_id: undercare_id,
        device_info: data.device_info,
        request_credential: data.request_credential,
        return_profile_pic: true,
        chinese_name_image_rgb: 0,
        chinese_name_image_font_size: 24,
        timestamp: serverTime
    }
    let url = API_PATH + API_NAME_UndercareRequest
    var str = JSON.stringify(inputFormat);
    let parameters = await parametersFromData(str);

    return await SubmitRequest(url, parameters, "POST", true);
}

export async function getFeedbackQuestion(input){

    let data = info;

    let inputFormat = {
        //survey_version:"1",
        device_info: data.device_info,
        request_credential: data.request_credential,
        feedback_app_id: input.app_id,
    }
    var str = JSON.stringify(inputFormat);
    let parameters = new FormData();

    parameters.append('content', str);
    parameters.append('lang', info.lang);

    let url =  API_PATH + GW_34;
    return await SubmitRequest(url, parameters, "POST", false);
}

export async function submitFeedBack(input){
  let user_id = await AsyncStorage.getItem("user_id");
  let user_is_login = await AsyncStorage.getItem("user_is_login");
  let feedback_user_id = "";

  if (user_id != null && user_is_login == 'Login'){
    feedback_user_id = user_id;
  }

  let inputFormat = {
      user_id:feedback_user_id,
      device_uuid: info.device_info.device_uuid,
      feedback_user_name:input.name,
      feedback_user_email:input.email,
      survey_version:input.survey_version,
      feedback:input.feedback,
      device_info:info.device_info,
      request_credential: info.request_credential,
      feedback_app_id: input.app_id,
  }

  var str = JSON.stringify(inputFormat);
  let parameters = new FormData();

  parameters.append('content', str);

  let url =  API_PATH + GW_33;
  return await SubmitRequest(url, parameters, "POST", false);
}

async function telecarePermissionChecking(from_app_id, to_app_id, navigate_message) {
    if (Platform.OS === 'ios') {
        Permissions.check('camera').then(response => {
            if (response == "authorized") {
                Permissions.check('microphone').then(response => {
                    if (response == "authorized") {
                        openTelecare(from_app_id, to_app_id, navigate_message)
                    } else {
                        iOS_telecare_general_alert(from_app_id, to_app_id, navigate_message)
                    }
                })
            } else {
                iOS_telecare_general_alert(from_app_id, to_app_id, navigate_message)
            }
        })
    } else {
        let hasEnablePermission = false;
        let disabledPermission = '';
        let permissions = [
            PermissionsAndroid.PERMISSIONS.CAMERA,
            PermissionsAndroid.PERMISSIONS.RECORD_AUDIO
        ]
        
        for (let i=0;i<permissions.length;i++){
            hasEnablePermission = await PermissionsAndroid.check(permissions[i])
            
            if (!hasEnablePermission){
                if (permissions[i] == PermissionsAndroid.PERMISSIONS.CAMERA) {
                    disabledPermission += 'camera,';
                } else if (permissions[i] == PermissionsAndroid.PERMISSIONS.RECORD_AUDIO){
                    disabledPermission += 'microphone,';
                }
            } 
        }

        if (disabledPermission == ''){
            hasEnablePermission = true;
        } else {
            hasEnablePermission = false;
        }

        if (!hasEnablePermission){
            var title = ""
            var message = ""
            var ok_button = ""

            if (HAGo_getLanguage() == "en") {
                title = "Preparation for HA Go tele-consultation"
                message = "Please ensure HA Go has permission to access your mobile device:\n1. Camera\n2. Microphone\n\nNote: Audio or video recording will not be made during the tele-consultation."
                ok_button = "OK"
            } else {
                title = "HA Go視像診症準備"
                message = "請確保你的流動裝置已授權使用權限，以進行視像診症：\n1. 相機功能\n2. 語音功能\n\n注意：在視像診症過程中，HA Go並不會進行任何錄影或錄音。"
                ok_button = "確定"
            }

            Alert.alert(
                title,
                message,
                [
                    {text: ok_button, onPress: async() => {
                        let permissions = [
                            PermissionsAndroid.PERMISSIONS.CAMERA,
                            PermissionsAndroid.PERMISSIONS.RECORD_AUDIO
                        ]
                        if (await PermissionUtil.requestPermissions(permissions, true)){
                            openTelecare(from_app_id, to_app_id, navigate_message)
                        }

                    }}
                ],
                {cancelable: false}
            );
        } else {
            openTelecare(from_app_id, to_app_id, navigate_message)
        }
    }
}

function iOS_telecare_general_alert(from_app_id, to_app_id, navigate_message) {
    var title = ""
    var message = ""
    var permission_title = ""
    var camera_message = ""
    var microphone_message = ""
    var ok_button = ""
    var setting_button = ""
    var cancel_button = ""

    if (HAGo_getLanguage() == "en") {
        title = "Preparation for HA Go tele-consultation"
        message = "Please ensure HA Go has permission to access your mobile device:\n1. Camera\n2. Microphone\n\nNote: Audio or video recording will not be made during the tele-consultation."
        permission_title = "HA Go cannot proceed tele-consultation"
        camera_message = "Please tap 'Settings' and turn on your camera of your mobile device."
        microphone_message = "Please tap 'Settings' and turn on your microphone of your mobile device."
        ok_button = "OK"
        setting_button = "Settings"
        cancel_button = "Cancel"
    } else {
        title = "HA Go視像診症準備"
        message = "請確保你的流動裝置已授權使用權限，以進行視像診症：\n1. 相機功能\n2. 語音功能\n\n注意：在視像診症過程中，HA Go並不會進行任何錄影或錄音。"
        permission_title = "HA Go未能進行視像診症"
        camera_message = "請按「設定」開啟流動裝置的相機。"
        microphone_message = "請按「設定」開啟流動裝置的咪高風。"
        ok_button = "確定"
        setting_button = "設定"
        cancel_button = "取消"
    }

    Alert.alert(
        title,
        message,
        [
            {text: ok_button, onPress: async() => {
                Permissions.request('camera').then(response => {
                    if (response == "denied") {
                        Alert.alert(
                            permission_title,
                            camera_message,
                            [{
                                text: setting_button,
                                onPress:() => {
                                    Linking.openURL('app-settings:');
                                }
                            },
                            {
                                text: cancel_button,
                                onPress:()=>{},
                                style: 'cancel'
                            }],
                            { cancelable: false }
                        )
                } else {
                    Permissions.request('microphone').then(response => {        
                        if (response == "denied") {
                            Alert.alert(
                                permission_title,
                                microphone_message,
                                [{
                                    text: setting_button,
                                    onPress:() => {
                                    Linking.openURL('app-settings:');
                                    }
                                },
                                {
                                    text: cancel_button,
                                    onPress:()=>{},
                                    style: 'cancel'
                                }],
                                { cancelable: false }
                            )
                        } else {
                            openTelecare(from_app_id, to_app_id, navigate_message)
                        }
                    })
                }
            })
          }}
        ],
        {cancelable: false}
    );
}

async function openTelecare(from_app_id, to_app_id, navigate_message) {
	let navigate_message_json = JSON.parse(navigate_message)

	navigate_message_json.sdk_key = "SDK_KEY"
	navigate_message_json.sdk_secret = "SDK_SECRET"
	navigate_message_json.room_id = "ROOM_ID"
	navigate_message_json.room_password = "ROOM_PW"

	Alert.alert(
		'',
		JSON.stringify(navigate_message_json),
		[{
			text: HAGo_getLanguage() == 'en'?'OK':'確定',
			onPress: () => {}
		}],
		{ cancelable: false }
	)
}

