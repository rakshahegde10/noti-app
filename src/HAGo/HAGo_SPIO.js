﻿//20191105/M/Paul WONG/v1.1.8 - Support Carer mode

import React, {Component} from 'react';
import {StatusBar, Platform, StyleSheet, Text, View , Dimensions,TouchableOpacity,Image, AsyncStorage} from 'react-native';
import { NativeModules, ImageBackground } from "react-native";
import SInfo from 'react-native-sensitive-info';

const {width, height} = Dimensions.get('window');

var Chinese_name = ''
var English_name = ''

import { HAGo_getSetting, HAGo_getLanguage, HAGo_isUndercareMode, HAGo_getUndercareInfo, HAGo_getFontSize } from './HAGo_authentication'

import PropTypes from 'prop-types';

const imageWidth = 30;

export class Base64TextView extends Component {

    static propTypes = {
        chinese: PropTypes.string,
        english: PropTypes.string,
        textStyle: PropTypes.style,
        ImageStyle: PropTypes.style,
        height: PropTypes.number,
    }

    constructor(props) {
        super(props);

        this.state = {
            imgHeight: this.props.height,
            imgWidth: 0,
        }
    }

    componentDidMount() {
        this.getChineseNameSize()
    }

    componentDidUpdate(prevProps) {
        if (prevProps.chinese !== this.props.chinese) {
            this.getChineseNameSize()
        }
    }

    getChineseNameSize() {
        try {
            let baseImg = this.props.chinese ? `data:image/png;base64,${this.props.chinese}` : ''

            Image.getSize(baseImg, (width, height) => {
                try {
                    var temp = width / (height / this.props.height)

                    this.setState({
                        imgHeight: this.props.height,
                        imgWidth: temp,
                    });
                } catch (e) {
                }
            });
        } catch (e) {
        }
    }

    render() {
        let baseImg = this.props.chinese ? `data:image/png;base64,${this.props.chinese}` : ''

        return (
            (HAGo_getLanguage() == 'en' || this.props.chinese == null || this.props.chinese == '') ?
                <Text style={this.props.textStyle}>
                    {this.props.english}
                </Text>
                :
                (this.state.imgHeight > 0 && this.state.imgWidth > 0) ?
                    <Image style={[this.props.ImageStyle, { height: this.state.imgHeight, width: this.state.imgWidth }]}
                        source={{ uri: baseImg }} />
                    :
                    <Image style={[this.props.ImageStyle]}
                        source={{ uri: baseImg }} />
        );
    }
}

export function normalizeFontSize(fontsize) {
    let fontScale = getFontScale();
    return Math.round(fontsize*fontScale);
}

export function getFontScale() {
    let fontSize = HAGo_getFontSize()

    var fontScale = 1

    switch(fontSize) {
        case 'small':
            fontScale = 0.8
            break;
        case 'normal':
            fontScale = 1
            break;
        case 'large':
            fontScale = 1.2
            break;
    }

    return fontScale;
}

export default class HAGo_SPIO extends React.Component{

    constructor(props) {
        super(props);
    }
    
    render() {
        let undercare_info = HAGo_getUndercareInfo()
        
        let bgColor = HAGo_isUndercareMode() ? '#008A20' : '#F00'

        let photoSource = undercare_info.undercare_profile_pic != null ? `data:image/png;base64,${undercare_info.undercare_profile_pic}` : '';
        let nicknameContent = undercare_info.undercare_nickname ? undercare_info.undercare_nickname : "";

        return (
            HAGo_isUndercareMode() ?
                (
                    <View style={{ flexDirection: 'row', padding: 16, backgroundColor: bgColor }}>
                        {photoSource == "" ? <Image source={require('./images/default_userIcon.png')} style={styles.profilePic} /> :
                            <Image source={{ uri: photoSource }} style={styles.profilePic} />}

                        {nicknameContent == "" ? <View /> :
                            <Text style={[styles.userNickNameTxtStyle, { fontSize: normalizeFontSize(18), maxWidth: normalizeFontSize(18) * 5 }]}>
                                {nicknameContent}
                            </Text>}

                        {nicknameContent == "" ? <View /> :
                            <Text style={[styles.userNickNameTxtStyle]}>
                                {" － "}
                            </Text>}

                        <Base64TextView
                            chinese={undercare_info.undercare_chinese_name}
                            english={undercare_info.undercare_english_name}
                            textStyle={[styles.userNameTxtStyle, { fontSize: normalizeFontSize(18) }]}
                            ImageStyle={[styles.userNameImgStyle, { height: 22 * getFontScale(), width: 100 }]}
                            height={22 * getFontScale()} />
                    </View>
                )
            :
            null
        )
    }
}

const styles = StyleSheet.create({
    profilePic: {
        width: imageWidth,
        height: imageWidth,
        alignSelf: "center",
        borderRadius: imageWidth / 2,
        borderWidth: 1,
        marginRight: 15,
        borderColor: '#fff',
        overflow: 'hidden',
    },
    userNameTxtStyle: {
        flexShrink: 1,
        color: '#fff',
        fontSize: 20,
        alignSelf: 'center',
        flexWrap: 'wrap',
    },
    userNameImgStyle: {
        tintColor: '#FFF',
        resizeMode: 'stretch',
        alignSelf: 'center',
    },
    userNickNameTxtStyle: {
        maxWidth: 80,
        flexShrink: 1,
        color: '#fff',
        fontSize: 16,
        alignSelf: 'center',
        flexWrap: 'wrap',
    },
    userNickNameImgStyle: {
        tintColor: '#fff',
        height: 22 * getFontScale(),
        resizeMode: 'stretch'
    },
});

