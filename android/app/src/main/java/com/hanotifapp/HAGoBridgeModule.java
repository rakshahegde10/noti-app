package com.hanotifapp;

import android.app.Activity;
import android.content.SharedPreferences;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;



public class HAGoBridgeModule extends ReactContextBaseJavaModule {

    private static final String HAGO_SHARED_PREF = "HAGO_ASYNCSTORAGE";

    public HAGoBridgeModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "HAGoBridge";
    }

    @ReactMethod
    public void setNativeStorage(String key, String value) {
        SharedPreferences prefs = getReactApplicationContext().getSharedPreferences(HAGO_SHARED_PREF, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }

    @ReactMethod
    public void getNativeStorage(String key, Promise promise) {
        SharedPreferences prefs = getReactApplicationContext().getSharedPreferences(HAGO_SHARED_PREF, Activity.MODE_PRIVATE);
        promise.resolve(prefs.getString(key, ""));
    }

    @ReactMethod
    public void removeNativeStorage(String key) {
        SharedPreferences prefs = getReactApplicationContext().getSharedPreferences(HAGO_SHARED_PREF, Activity.MODE_PRIVATE);
        prefs.edit().remove(key).commit();
    }

}