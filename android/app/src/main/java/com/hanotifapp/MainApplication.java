package com.hanotifapp;

import android.app.Application;
import android.content.Context;

import com.AlexanderZaytsev.RNI18n.RNI18nPackage;
import com.RNFetchBlob.RNFetchBlobPackage;
import com.RNRSA.RNRSAPackage;
import com.actionsheet.ActionSheetPackage;
import com.chirag.RNMail.RNMail;
import com.christopherdro.RNPrint.RNPrintPackage;
import com.facebook.react.BuildConfig;
import com.facebook.react.ReactApplication;
import com.facebook.react.shell.MainReactPackage;
import com.gantix.JailMonkey.JailMonkeyPackage;
import com.github.yamill.orientation.OrientationPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.lufinkey.react.eventemitter.RNEventEmitterPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.soloader.SoLoader;
import com.th3rdwave.safeareacontext.SafeAreaContextPackage;
import com.wheelpicker.WheelPickerPackage;

import org.devio.rn.splashscreen.SplashScreenReactPackage;
import org.wonday.pdf.RCTPdfView;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;

import br.com.classapp.RNSensitiveInfo.RNSensitiveInfoPackage;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost =
      new ReactNativeHost(this) {
        @Override
        public boolean getUseDeveloperSupport() {
          return BuildConfig.DEBUG;
        }

        @Override
        protected List<ReactPackage> getPackages() {
            return Arrays.<ReactPackage>asList(new MainReactPackage(),
                    new VectorIconsPackage(),
                    new ActionSheetPackage(),
                    new RNEventEmitterPackage(),
                    new WheelPickerPackage(),
                    new OrientationPackage(),
                    new RNFetchBlobPackage(),
                    new RNPrintPackage(),
                    new RCTPdfView(),
                    new RNMail(),
                    new JailMonkeyPackage(),
                    new SplashScreenReactPackage(),
                    new RNSensitiveInfoPackage(),
                    new RNRSAPackage(),
                    new RNI18nPackage(),
                    new RNDeviceInfo(),
                    new HAGoBridgeReactPackage(),
                    new SafeAreaContextPackage(),
            new RNGestureHandlerPackage()
            );
        }

        @Override
        protected String getJSMainModuleName() {
          return "index";
        }
      };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
    initializeFlipper(this, getReactNativeHost().getReactInstanceManager());
  }

  /**
   * Loads Flipper in React Native templates. Call this in the onCreate method with something like
   * initializeFlipper(this, getReactNativeHost().getReactInstanceManager());
   *
   * @param context
   * @param reactInstanceManager
   */
  private static void initializeFlipper(
      Context context, ReactInstanceManager reactInstanceManager) {
    if (BuildConfig.DEBUG) {
      try {
        /*
         We use reflection here to pick up the class that initializes Flipper,
        since Flipper library is not available in release mode
        */
        Class<?> aClass = Class.forName("com.hanotifapp.ReactNativeFlipper");
        aClass
            .getMethod("initializeFlipper", Context.class, ReactInstanceManager.class)
            .invoke(null, context, reactInstanceManager);
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      } catch (NoSuchMethodException e) {
        e.printStackTrace();
      } catch (IllegalAccessException e) {
        e.printStackTrace();
      } catch (InvocationTargetException e) {
        e.printStackTrace();
      }
    }
  }
}
