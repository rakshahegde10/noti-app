#Global script
# sudo cp /Users/administrator/Documents/HAGoIntegrationScript.sh /usr/local/bin

# integration
ROOT=$1
UPDATED_FILES=$2

echo "after_npminstall"
echo "UPDATED_FILES: $UPDATED_FILES"

## iOS Pod integration
declare -a file_arr=("./node_modules/react-native-payments/ReactNativePayments.podspec")
## now loop through the above array
echo "********** iOS Pod integration STARTED *************"
for integration_file in "${file_arr[@]}"
do
	if [ -f "$integration_file" ]
		then
			echo "Copying $integration_file..."
			cp -rf -p "$UPDATED_FILES/$integration_file" "$integration_file"
		else
			echo "local $integration_file not found"
			exit
		fi
done

echo "********** iOS Pod integration FINISHED *************"


## Android X integration
declare -a file_arr=("./node_modules/react-native-add-calendar-event/android/src/main/java/com/vonovak/Utils.java" "./node_modules/react-native-add-calendar-event/android/src/main/java/com/vonovak/AddCalendarEventModule.java" "./node_modules/react-native-camera-kit/android/src/main/java/com/wix/RNCameraKit/camera/CameraViewManager.java" "./node_modules/react-native-camera-kit/android/src/main/java/com/wix/RNCameraKit/camera/CameraView.java" "./node_modules/react-native-camera-kit/android/src/main/java/com/wix/RNCameraKit/camera/barcode/BarcodeScanner.java" "./node_modules/react-native-camera-kit/android/src/main/java/com/wix/RNCameraKit/camera/barcode/BarcodeFrame.java" "./node_modules/react-native-camera-kit/android/src/main/java/com/wix/RNCameraKit/camera/permission/CameraPermission.java" "./node_modules/react-native-camera-kit/android/src/main/java/com/wix/RNCameraKit/gallery/GalleryView.java" "./node_modules/react-native-camera-kit/android/src/main/java/com/wix/RNCameraKit/gallery/GalleryAdapter.java" "./node_modules/react-native-camera-kit/android/src/main/java/com/wix/RNCameraKit/Utils.java" "./node_modules/react-native-camera-kit/android/src/main/java/com/wix/RNCameraKit/gallery/NativeGalleryModule.java" "./node_modules/react-native-camera-kit/android/src/main/java/com/wix/RNCameraKit/gallery/permission/StoragePermission.java" "./node_modules/react-native-camera-kit/android/src/main/java/com/wix/RNCameraKit/RNCameraKitPackage.java" "./node_modules/react-native-camera-kit/android/src/main/java/com/wix/RNCameraKit/SaveImageTask.java" "./node_modules/react-native-contacts/android/src/main/java/com/rt2zz/reactnativecontacts/ReactNativeContacts.java" "./node_modules/react-native-contacts/android/src/main/java/com/rt2zz/reactnativecontacts/ContactsManager.java" "./node_modules/react-native-contacts/android/src/main/java/com/rt2zz/reactnativecontacts/ContactsProvider.java" "./node_modules/react-native-fingerprint-android/android/src/main/java/io/jari/fingerprint/FingerprintModule.java" "./node_modules/react-native-payments/android/src/main/java/com/reactnativepayments/ReactNativePaymentsModule.java")
## now loop through the above array
echo "********** Android X integration STARTED *************"
for integration_file in "${file_arr[@]}"
do
	if [ -f "$integration_file" ]
		then
			echo "Copying $integration_file..."
			cp -rf -p "$UPDATED_FILES/$integration_file" "$integration_file"
		else
			echo "local $integration_file not found"
			exit
		fi
done
echo "********** Android X integration FINISHED *************"

## Android .xml integration
declare -a file_arr=("./node_modules/react-native-call-detection/android/src/main/AndroidManifest.xml")
## now loop through the above array
echo "********** Android .xml integration STARTED *************"
for integration_file in "${file_arr[@]}"
do
	if [ -f "$integration_file" ]
		then
			echo "Copying $integration_file..."
			cp -rf -p "$UPDATED_FILES/$integration_file" "$integration_file"
		else
			echo "local $integration_file not found"
			exit
		fi
done
echo "********** Android .xml integration FINISHED *************"

