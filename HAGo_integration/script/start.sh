# Please run "./integration/script/main.sh" in terminal to start the integration
ROOT="$PWD"
UPDATED_FILES="./HAGo_integration/"
SCRIPT_FOLDER="./HAGo_integration/script/"

echo "ROOT PATH: $ROOT"

# remove node_modules in project
if ! [ -d "$ROOT/node_modules/" ]
then
    rm -rf "$ROOT/node_modules/"
    echo "Removing local node_modules folder"
fi

# remove Pod files in project
if ! [ -d "$ROOT/ios/Pods/" ]
then
    rm -rf "$ROOT/ios/Pods/"
    echo "Removing local Pods folder"
fi

# install node_modules
npm install

# integration after the node_modules were installed
sh "$SCRIPT_FOLDER/after_npminstall.sh" "$ROOT" "$UPDATED_FILES"

# install pods files for iOS
cd ./iOS/ && pod install
cd ../

# integration for pod files
sh "$SCRIPT_FOLDER/after_podinstall.sh" "$ROOT" "$UPDATED_FILES"

