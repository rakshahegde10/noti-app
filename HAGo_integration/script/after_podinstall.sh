#Global script
# sudo cp /Users/administrator/Documents/HAGoIntegrationScript.sh /usr/local/bin

# integration
ROOT=$1
UPDATED_FILES=$2

## iOS Pod integration

## Flipper libraries - add "${PODS_ROOT}/Headers/Public/OpenSSL_Universal" to header search path
if [ -f "$ROOT/ios/Pods/Target Support Files/Flipper/Flipper.xcconfig" ]
		then
			if ! grep -q "OpenSSL_Universal" "$ROOT/ios/Pods/Target Support Files/Flipper/Flipper.xcconfig";
				then
					echo "OpenSSL_Universal is added in Flipper.xcconfig"
					sed -i -e 's/\"\$(PODS_ROOT)\/Flipper\-DoubleConversion\"/\"\$(PODS_ROOT)\/Flipper\-DoubleConversion\" \"${PODS_ROOT}\/Headers\/Public\/OpenSSL_Universal\"/g' "$ROOT/ios/Pods/Target Support Files/Flipper/Flipper.xcconfig"
				else
					echo "OpenSSL_Universal is already added in Flipper.xcconfig"
			fi
		else
			echo "Flipper.xcconfig not found"
			exit
		fi

## Flipper libraries - add "${PODS_ROOT}/Headers/Public/OpenSSL_Universal" to header search path
if [ -f "$ROOT/ios/Pods/Target Support Files/Flipper-Folly/Flipper-Folly.xcconfig" ]
		then
			if ! grep -q "OpenSSL_Universal" "$ROOT/ios/Pods/Target Support Files/Flipper-Folly/Flipper-Folly.xcconfig";
				then
					echo "OpenSSL_Universal is added in Flipper-Folly.xcconfig"
					sed -i -e 's/\"\$(PODS_ROOT)\/Flipper\-DoubleConversion\"/\"\$(PODS_ROOT)\/Flipper\-DoubleConversion\" \"${PODS_ROOT}\/Headers\/Public\/OpenSSL_Universal\"/g' "$ROOT/ios/Pods/Target Support Files/Flipper-Folly/Flipper-Folly.xcconfig"
			else
				echo "OpenSSL_Universal is already added in Flipper-Folly.xcconfig"
			fi
		else
			echo "Flipper-Folly.xcconfig not found"
			exit
		fi	

## Flipper libraries - add "${PODS_ROOT}/Headers/Public/OpenSSL_Universal" to header search path
if [ -f "$ROOT/ios/Pods/Target Support Files/Folly/Folly.xcconfig" ]
		then
			if ! grep -q "OpenSSL_Universal" "$ROOT/ios/Pods/Target Support Files/Folly/Folly.xcconfig";
				then
					echo "OpenSSL_Universal is added in Folly.xcconfig"
					sed -i -e 's/\"\$(PODS_ROOT)\/DoubleConversion\"/\"\$(PODS_ROOT)\/DoubleConversion\" \"${PODS_ROOT}\/Headers\/Public\/OpenSSL_Universal\"/g' "$ROOT/ios/Pods/Target Support Files/Folly/Folly.xcconfig"
				else
					echo "OpenSSL_Universal is already added in Folly.xcconfig"
			fi
		else
			echo "Folly.xcconfig not found"
			exit
		fi	

## Flipper libraries - add "${PODS_ROOT}/Headers/Public/OpenSSL_Universal" to header search path
if [ -f "$ROOT/ios/Pods/Target Support Files/Flipper-RSocket/Flipper-RSocket.xcconfig" ]
		then
			if ! grep -q "OpenSSL_Universal" "$ROOT/ios/Pods/Target Support Files/Flipper-RSocket/Flipper-RSocket.xcconfig";
				then
					echo "OpenSSL_Universal is added in Flipper-RSocket"
					sed -i -e 's/\"\$(PODS_ROOT)\/glog\"/\"\$(PODS_ROOT)\/glog\" \"${PODS_ROOT}\/Headers\/Public\/OpenSSL_Universal\"/g' "$ROOT/ios/Pods/Target Support Files/Flipper-RSocket/Flipper-RSocket.xcconfig"
				else
					echo "OpenSSL_Universal is already added in Flipper-RSocket.xcconfig"
			fi
		else
			echo "Flipper-RSocket.xcconfig not found"
			exit
		fi	
echo "********** iOS Pod integration FINISHED *************"