/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import 'react-native-gesture-handler';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import PatienLibLandingScreen from './src/HAGo/HAGo_PatientLibraryLanding';
import NotificationScreen from './src/home/NotificationScreen';
import NotificationDetailScreen from './src/home/NotificationDetailScreen'
import { HAGo_init, HAGo_getLanguage } from './src/HAGo/HAGo_authentication';
import I18n from "./src/language";

export default class App extends Component {

  constructor(props) {
    super(props);
    HAGo_init(props);
    I18n.locale = HAGo_getLanguage();
  }

  render() {
    const Stack = createStackNavigator()
    return (
    <NavigationContainer>
       <Stack.Navigator initialRouteName="PatientLibLanding">
       <Stack.Screen name="PatientLibLanding" component={PatienLibLandingScreen}  />
       <Stack.Screen name="NotificationLanding" component={NotificationScreen}  />
       <Stack.Screen name="Details" component={NotificationDetailScreen}  />
       </Stack.Navigator>
    </NavigationContainer>
    )
  }
}
